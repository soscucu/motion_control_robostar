// TI File $Revision: /main/4 $
// Checkin $Date: July 30, 2007   14:15:53 $
//###########################################################################
//
// FILE:	DSP2833x_Adc.c
//
// TITLE:	DSP2833x ADC Initialization & Support Functions.
//
//###########################################################################
// $TI Release: DSP2833x Header Files V1.00 $
// $Release Date: September 7, 2007 $
//###########################################################################

#include "DSP2834x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2834x_Examples.h"   // DSP2833x Examples Include File
#include "MPLD.h"
#include "variable.h"
//#define ADC_usDELAY  5000L

//---------------------------------------------------------------------------
// InitAdc:
//---------------------------------------------------------------------------
// This function initializes ADC to a known state.
//
int ADCsetVal;

void InitAdc(void){
	ADCsetVal= (int)( 1.65 / 2.5*(float)(0x03FF));	// make Reference voltage 1.65V(1.596V)

	AD_RD0 = 0x101;
	AD_RD0 = ADCsetVal;
	AD_RD1 = 0x101;
	AD_RD1 = ADCsetVal;
	AD_RD2 = 0x101; 
	AD_RD2 = ADCsetVal;			
}

//===========================================================================
// End of file.
//===========================================================================


void ADC(void){
	AD_RD0 = 0x100;
	AD_RD1 = 0x100;
	AD_RD2 = 0x100;


	delaycc(0.005e-6);

	//	conversion start		
	ADC0_SOC_START;
	delaycc(0.005e-6);			
	ADC0_SOC_END;

	ADC1_SOC_START;
	delaycc(0.005e-6);			
	ADC1_SOC_END;

	ADC2_SOC_START;	
	delaycc(0.005e-6);
	ADC2_SOC_END;


	while(ADC0_BUSY);
	ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD0_CHA0 : Ia_sen
//	ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD0_CHB0 : Ib_sen
		
	while(ADC1_BUSY);
	ADCH[2] = (AD_RD1 - 0x0800) & 0x0FFF;			//		AD1_CHA0 : Ic_sen
	ADCH[3] = (AD_RD1 - 0x0800) & 0x0FFF;			//		AD1_CHB0 : Vdc

	while(ADC2_BUSY);
	ADCH[4] = (AD_RD2 - 0x0800) & 0x0FFF;			//		AD2_CHA0 : Temp_sen	(temperture)
//	ADCH[5] = (AD_RD2 - 0x0800) & 0x0FFF;			//		AD2_CHB0 : Idc
/*
	AD_RD0 = 0x500;
	AD_RD1 = 0x500;
	AD_RD2 = 0x500;

	delaycc(0.005e-6);

	//	conversion start	
	ADC0_SOC_START;
	delaycc(0.005e-6);
	ADC0_SOC_END;

	ADC1_SOC_START;
	delaycc(0.005e-6);
	ADC1_SOC_END;

	ADC2_SOC_START;		
	delaycc(0.005e-6);
	ADC2_SOC_END;

	while(ADC0_BUSY);

	ADCH[6] = (AD_RD0 - 0x0800) & 0x0FFF;			//		Vvn
	ADCH[7] = (AD_RD0 - 0x0800) & 0x0FFF;			//		Dummy

	while(ADC1_BUSY);

	ADCH[8] = (AD_RD1 - 0x0800) & 0x0FFF;			//		Vwn
	ADCH[9] = (AD_RD1 - 0x0800) & 0x0FFF;			//		Dummy


	while(ADC2_BUSY);

	ADCH[10] = (AD_RD2 - 0x0800) & 0x0FFF;			//		Dummy
	ADCH[11] = (AD_RD2 - 0x0800) & 0x0FFF;			//		Dummy

	AD_RD2 = 0x900;
	delaycc(0.005e-6);

	ADC2_SOC_START;
	delaycc(0.005e-6);
	ADC2_SOC_END;

	while(ADC2_BUSY);

	ADCH[12] = (AD_RD2 - 0x0800) & 0x0FFF;			//		Dummy
	ADCH[13] = (AD_RD2 - 0x0800) & 0x0FFF;			//		Dummy
*/
}



