/***************************************************************
	FILTER.c
	generic 1st- and 2nd-order filter
	
	1st-order filter
	void IIR1Init(IIR1 *p_gIIR, const float sample_time);
	void IIR1CoeffInit(IIR1 *p_gIIR, const float sample_time); 
	float IIR1Update(IIR1 *p_gIIR, const float input);

	2nd-order filter
	void IIR2Init(IIR2 *p_gIIR, const float sample_time);
	void IIR2CoeffInit(IIR2 *p_gIIR, const float sample_time); 
	float IIR2Update(IIR2 *p_gIIR, const float input);

	programmed by Yo-chan Son, Oct 2000.
	copyright (c) 1991, 2000 by EEPEL, SNU, SEOUL, KOREA
	All Rights Reserved.
	modified by T.S.Kwon, 2008
	1st-order filter : 
	void IIR1CoeffInit(IIR1 *p_gIIR, float w0 );
	void IIR1Init(IIR1 *p_gIIR, float w0 );
	float IIR1Update(IIR1 *p_gIIR, const float input );

	2nd-order filter : 
	void IIR2CoeffInit(IIR1 *p_gIIR, float w0, float zeta );
	void IIR2Init(IIR2 *p_gIIR, float w0, float zeta );
	float IIR2Update(IIR2 *p_gIIR, const float input);
****************************************************************/
#include "filter.h"
#include "comp.h"


void initiateIIR1(IIR1 *p_gIIR, int type, float w0, float Tsam)
{
	float a0, b0, b1;
	float INV_alpha;

	// Continuous-time Filter Coefficients
	p_gIIR->w0 = w0;
	p_gIIR->type = type;
	p_gIIR->delT = Tsam;

	a0 = w0;
	switch(type)
	{
		case K_LPF:
			b0 = w0;
			b1 = 0;
			break;
		case K_HPF: 
			b0 = 0;
			b1 = (float)1;
			break;
		default:
		case K_ALLPASS:
			b0 = -w0;
			b1 = (float)1;
	}

	// Discrete-time Filter Coefficients
	INV_alpha = (float)1./((float)2 + Tsam*a0);
	p_gIIR->coeff[0] = ((float)2*b1 + Tsam*b0)*INV_alpha;
	p_gIIR->coeff[1] = (-(float)2*b1 + Tsam*b0)*INV_alpha;
	p_gIIR->coeff[2] = -(-(float)2 + Tsam*a0)*INV_alpha;
	p_gIIR->reg = 0.;
}
  
void UpdateGainIIR1(IIR1 *p_gIIR, int type, float w0, float Tsam)
{
	float a0, b0, b1;
	float INV_alpha;

	// Continuous-time Filter Coefficients
	p_gIIR->w0 = w0;
	p_gIIR->type = type;
	p_gIIR->delT = Tsam;

	a0 = w0;
	switch(type)
	{
		case K_LPF:
			b0 = w0;
			b1 = 0;
			break;
		case K_HPF: 
			b0 = 0;
			b1 = (float)1;
			break;
		default:
		case K_ALLPASS:
			b0 = -w0;
			b1 = (float)1;
	}

	// Discrete-time Filter Coefficients
	INV_alpha = (float)1./((float)2 + Tsam*a0);
	p_gIIR->coeff[0] = ((float)2*b1 + Tsam*b0)*INV_alpha;
	p_gIIR->coeff[1] = (-(float)2*b1 + Tsam*b0)*INV_alpha;
	p_gIIR->coeff[2] = -(-(float)2 + Tsam*a0)*INV_alpha;
	//p_gIIR->reg = 0.;
}


float IIR1Update(IIR1 *p_gIIR, float x)
{
	float y;

	y = p_gIIR->reg + p_gIIR->coeff[0]*x;
	p_gIIR->reg = p_gIIR->coeff[1]*x + p_gIIR->coeff[2]*y;

	return(y);
}

void initiateIIR2(IIR2 *p_gIIR, int type, float w0, float zeta, float Tsam)
{
	float a0, a1, b0, b1, b2;
	float INV_alpha;

	// Continuous-time Filter Coefficients
	p_gIIR->w0 = w0;
	p_gIIR->zeta = zeta;
	p_gIIR->delT =	Tsam;
	p_gIIR->type = type;

	a0 = w0*w0;
	a1 = 2*zeta*w0;
	switch(type)
	{
		case K_LPF:
			b0 = w0*w0;
			b1 = 0;
			b2 = 0;
			break;
		case K_HPF: 
			b0 = 0;
			b1 = 0;
			b2 = (float)1;   
			break;
		case K_BPF:
			b0 = 0;
			b1 = (float)2*zeta*w0;
			b2 = 0;
			break;
		case K_NOTCH:
			b0 = w0*w0;
			b1 = 0;
			b2 = (float)1;
			break;
		case K_ALLPASS:
		default:
			b0 = w0*w0;
			b1 = -(float)2*zeta*w0;
			b2 = (float)1;
	}

	// Discrete-time Filter Coefficients
	INV_alpha = (float)1./((float)4 + (float)2*Tsam*a1 + Tsam*Tsam*a0);
	p_gIIR->coeff[0] = ((float)4*b2 + (float)2*Tsam*b1 + Tsam*Tsam*b0)*INV_alpha;
	p_gIIR->coeff[1] = ((float)2*Tsam*Tsam*b0 - (float)8*b2)*INV_alpha;
	p_gIIR->coeff[2] = -((float)2*Tsam*Tsam*a0 - (float)8)*INV_alpha;
	p_gIIR->coeff[3] = ((float)4*b2 - (float)2*Tsam*b1 + Tsam*Tsam*b0)*INV_alpha;
	p_gIIR->coeff[4] = -((float)4 - (float)2*Tsam*a1 + Tsam*Tsam*a0)*INV_alpha;
	
	p_gIIR->reg[0] = 0;
	p_gIIR->reg[1] = 0;
	
}



void UpdateGainIIR2(IIR2 *p_gIIR, int type, float w0, float zeta, float Tsam)
{
	float a0, a1, b0, b1, b2;
	float INV_alpha;

	// Continuous-time Filter Coefficients
	p_gIIR->w0 = w0;
	p_gIIR->zeta = zeta;
	p_gIIR->delT =	Tsam;
	p_gIIR->type = type;

	a0 = w0*w0;
	a1 = 2*zeta*w0;
	switch(type)
	{
		case K_LPF:
			b0 = w0*w0;
			b1 = 0;
			b2 = 0;
			break;
		case K_HPF: 
			b0 = 0;
			b1 = 0;
			b2 = (float)1;   
			break;
		case K_BPF:
			b0 = 0;
			b1 = (float)2*zeta*w0;
			b2 = 0;
			break;
		case K_NOTCH:
			b0 = w0*w0;
			b1 = 0;
			b2 = (float)1;
			break;
		case K_ALLPASS:
		default:
			b0 = w0*w0;
			b1 = -(float)2*zeta*w0;
			b2 = (float)1;
	}

	// Discrete-time Filter Coefficients
	INV_alpha = (float)1./((float)4 + (float)2*Tsam*a1 + Tsam*Tsam*a0);
	p_gIIR->coeff[0] = ((float)4*b2 + (float)2*Tsam*b1 + Tsam*Tsam*b0)*INV_alpha;
	p_gIIR->coeff[1] = ((float)2*Tsam*Tsam*b0 - (float)8*b2)*INV_alpha;
	p_gIIR->coeff[2] = -((float)2*Tsam*Tsam*a0 - (float)8)*INV_alpha;
	p_gIIR->coeff[3] = ((float)4*b2 - (float)2*Tsam*b1 + Tsam*Tsam*b0)*INV_alpha;
	p_gIIR->coeff[4] = -((float)4 - (float)2*Tsam*a1 + Tsam*Tsam*a0)*INV_alpha;
	
	//p_gIIR->reg[0] = 0;
	//p_gIIR->reg[1] = 0;
}




float IIR2Update(IIR2 *p_gIIR, const float x)
{
	float y;

	y = p_gIIR->reg[0] + p_gIIR->coeff[0]*x;
	p_gIIR->reg[0] = p_gIIR->reg[1] + p_gIIR->coeff[1]*x + p_gIIR->coeff[2]*y;
	p_gIIR->reg[1] = p_gIIR->coeff[3]*x + p_gIIR->coeff[4]*y;

	return(y);
}

	IIR1 Filter_WrH1;
	IIR1 Filter_WrH2;
	IIR1 Filter_Edse;
	IIR1 Filter_Eqse;
    IIR1 Filter_Vdiff;
	
//	IIR1 Filter_Idse;
	IIR1 Filter_Vdse;
	IIR1 Filter_Vqse;
	IIR1 Filter_Vdse_flt;
	IIR1 Filter_Vqse_flt;

//	IIR1 Filter_Iqse;
	IIR1 Filter_Idc_sen;

	IIR1 Filter_Idss;
	IIR1 Filter_Iqss;
	IIR1 Filter_Idsc;
	IIR1 Filter_Iqsc;


	IIR2 Filter_Idseh;
	IIR2 Filter_Iqseh;

	IIR1 Filter_Ke_d;
	IIR1 Filter_Ke_q;
	
	IIR1 Filter_Ke_d_sum;
	IIR1 Filter_Ke_q_sum;


	IIR2 Filter_test;
	IIR2 Filter_Pt;
	IIR2 Filter_Pinv;

	IIR2 Filter_Idse;
	IIR2 Filter_Iqse;

	IIR2 Filter_J_find;