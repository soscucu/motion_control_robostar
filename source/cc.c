// TI File $Revision: /main/8 $
// Checkin $Date: August 10, 2007   09:04:44 $
//###########################################################################
//
// FILE:    cc.c
//
//
// ASSUMPTIONS:
//
//    This program requires the DSP2833x header files.
//
//    Other then boot mode configuration, no other hardware configuration
//    is required.
//
//    As supplied, this project is configured for "boot to SARAM"
//    operation.  The 2833x Boot Mode table is shown below.
//    For information on configuring the boot mode of an eZdsp,
//    please refer to the documentation included with the eZdsp,
//
//       $Boot_Table:
//
//         GPIO87   GPIO86     GPIO85   GPIO84
//          XA15     XA14       XA13     XA12
//           PU       PU         PU       PU
//        ==========================================
//            1        1          1        1    Jump to Flash
//            1        1          1        0    SCI-A boot
//            1        1          0        1    SPI-A boot
//            1        1          0        0    I2C-A boot
//            1        0          1        1    eCAN-A boot
//            1        0          1        0    McBSP-A boot
//            1        0          0        1    Jump to XINTF x16
//            1        0          0        0    Jump to XINTF x32
//            0        1          1        1    Jump to OTP
//            0        1          1        0    Parallel GPIO I/O boot
//            0        1          0        1    Parallel XINTF boot
//            0        1          0        0    Jump to SARAM	    <- "boot to SARAM"
//            0        0          1        1    Branch to check boot mode
//            0        0          1        0    Boot to flash, bypass ADC cal
//            0        0          0        1    Boot to SARAM, bypass ADC cal
//            0        0          0        0    Boot to SCI-A, bypass ADC cal
//                                              Boot_Table_End$
//
// DESCRIPTION:
//
//
//###########################################################################
// $TI Release: DSP2833x Header Files V1.00 $
// $Release Date: September 7, 2007 $
//###########################################################################


#include <math.h>
#include "DSP2834x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2834x_Examples.h"   // DSP2833x Examples Include File
#include "MPLD.h"
#include "da.h"
#include "cc.h"
#include "sc.h"
#include "comp.h"
#include "variable.h"
#include "filter.h"
#include "fault.h"
#define ON 1
#define OFF 0
/* Variable for measuring CC Time */

SpdObs Obs1;

SC Inv1_SC;
DstObs Obsd;
TDOF TDOFV;


long ccTimer2start=0;
long ccTimer3start=0;
float Time_CC = 0.;
float Time_SL = 0.;

void makeOffsetTable( Motor *tmpM);
float NP_duty(float Van);
float  abc,test_fre=0.;
int epwm_test=0, relay_off=0,relay_time=1,fan_con=0;
int test_Van=0;




float Timer3start=0, Time_f=0;
float theta_e = 0;


float Vdse_ref=0;
float Vqse_ref=0;

float Vdse_ff = 0.;
float Vqse_ff = 0.;
float phs_ff = 0.;


float fault_cnt = 0.;

float Theta_IbyF = 0.;
float Thetam_enc = 0.;
float encoder_float = 0;
float eqep_offset_toogle = 0.;
float test_Wr = 0.;

int rotor_align = 0;

//IF
float fault_vdc = 0.;
float fault_current = 0.;
float fault_speed = 0.;
float current_torque = 0.;
int Freq_test1 = 15000;
int pc_mode = 0;
int sc_mode = 0;
int Is_predict_on = 0;

float sc_mode_float = 0.;
float pc_mode_float = 0.;

int SC_high_freq_inject = 0;

float sc_freq = 0.;
float thetar_inject = 0.;
float Fsw_set = 10000;
float Fcc_set = 1000.;
float Fsc_set = 50.;
float Fpc_set = 1.;

float Fsw_init = 15000.;
float Fcc_init = 1700.;
float Fcc_stop = 1700.;

float Fsw_stop = 15000.;
float Fsc_init = 50.;
float Fsc_stop = 50.;

float Fpc_init = 0.05;
float Fpc_stop = 0.05;	
float test_pwm_duty4 = 0.;
float test_pwm_duty5 = 0.;
float test_pwm_duty6 = 0.;

int sin_on = 0;
float temp_theta = 0.;
float sin_freq = 50.;
float sin_mag = 2.5;
float temp_deadA = 400.;
float temp_deadB = 400.;
float temp_deadC = 400.;


float sc_time = 0.;
float pc_time = 0.;
float Freq_prev = 0.;

float test_Vdse = 0.;
float test_Vqse = 0.;
float cc_cnt = 0.;
int cc_direction = 0;


int sc_cnt_max = 10.;
int sc_cnt = 0;

interrupt void cc(void)	
{	if(cc_cnt > 100) cc_direction = 0;
	else if(cc_cnt <0) cc_direction = 1;
	if(cc_direction) cc_cnt = cc_cnt + 1;
	else cc_cnt = cc_cnt - 1;


	if(SW1.Double_sampling_on) EPwm8Regs.ETSEL.bit.INTEN = 1;
	else EPwm8Regs.ETSEL.bit.INTEN = 0;		

	if (EPwm7Regs.ETFLG.bit.INT) {
		EpwmIntNum = 7.;


	//	test_pwm4 = pwm4;
	//	test_pwm5 = pwm5;
	//	test_pwm6 = pwm6;
		if(SW1.Variable_gain_on){
			if(SW1.PC_on){
				if(pc_mode == 7){
					Freq_test1 = Fsw_stop;
					Inv1.Fcc = Fcc_stop;
					Inv1.Fsc = Fsc_stop;
					Inv1.Fpc = Fpc_stop;
				}
				else {
					Freq_test1 = Fsw_set;
					Inv1.Fcc = Fcc_set;
					Inv1.Fsc = Fsc_set;
					Inv1.Fpc = Fpc_set;
				}	 

			}
			else if (SW1.SC_on){
				if((sc_mode == 1)||(sc_mode ==2)) {
					Freq_test1 = Fsw_init;
					Inv1.Fcc = Fcc_init;
					Inv1.Fsc = Fsc_init;
					Inv1.Fpc = Fpc_init;
				}
				else if(sc_mode == 3){
					Freq_test1 = Fsw_stop;
					Inv1.Fcc = Fcc_stop;
					Inv1.Fsc = Fsc_stop;
					Inv1.Fpc = Fpc_stop;
				}
				else{
					Freq_test1 = Fsw_set;
					Inv1.Fcc = Fcc_set;
					Inv1.Fsc = Fsc_set;
					Inv1.Fpc = Fpc_set;
				}
			}
			else{
				Freq_test1 = Fsw_set;
				Inv1.Fcc = Fcc_set;
				Inv1.Fsc = Fsc_set;
				Inv1.Fpc = Fpc_set;

			}
				
		}
		else {
			Freq_test1 = Fsw_set;
			Inv1.Fcc = Fcc_set;
			Inv1.Fsc = Fsc_set;
			Inv1.Fpc = Fpc_set;
		}
		if(Freq_test1 != Freq_prev) PWMCarrierFreqSetting(Freq_test1);
		Freq_prev = Freq_test1;
	} else if(EPwm8Regs.ETFLG.bit.INT) {
		EpwmIntNum = 8.;
	}


	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IERBackupCC = IER;
	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3|M_INT5;		/* HW fault & TINT0 */
	EINT;   /* Enable Global interrupt INTM */
	
	DeadbandSetting(Inv1.Comp_posA_DEAD,Inv1.Comp_posB_DEAD,Inv1.Comp_posC_DEAD);	

	ccTimer2start = ReadCpuTimer2Counter();

    if(SW1.Relay_on == ON) RELAY_ON;
    else RELAY_OFF;

	Timer3start = ReadCpuTimer2Counter();
  	Time_f = (float)(Timer3start - ReadCpuTimer2Counter())*SYSTEM_CLOCK_PRD;





/////////////////////////////////////////////////////////////
////////////////		ADC Conversion     //////////////////
/////////////////////////////////////////////////////////////

	delaycc(Tdelay);	/* 측정시간 확보를 위해 Tdelay만큼 측정시간을 미룸 */

	ADC();
	
	Inv1.Ia	= ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0] + Scalecur ;
//	Inv1.Ib	= ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1] + Scalecur ;
	Inv1.Ic	= ((float) ADCH[2]-OffsetAin[2]) * ScaleAin[2] + Scalecur ;
	Inv1.Ib = -(Inv1.Ia + Inv1.Ic);
	Vdc 	= ((float) ADCH[3]) * ScaleAin[3];
	
//	Inv1.Idc_sen	= ((float) ADCH[5]-OffsetAin[5]) * ScaleAin[5] + Scalecur;	
	temperture = (((float) ADCH[4]) * ScaleAin[4] - 59.0476 );

	half_Vdc 	= 0.5 * Vdc;
	if(ABS(Vdc) >= 1.) INV_Vdc = 1. / Vdc;
	else INV_Vdc = 1.;



////////////////////////////////////////////////////////////////////
////////////////		Phase current SW protection ////////////////
////////////////////////////////////////////////////////////////////	

	if (Vdc > Prot.VdcLimOver)								{ fault_vdc = Vdc; Fault.SW.bit.Vdc_Over = 1;								FaultProcess();}
	if( fabs(Inv1.Ia) > Prot.CurrentLim_inv1) 				{ fault_cnt++; if(fault_cnt >=5 ){fault_current = Inv1.Ia;  Fault.SW.bit.I_u = 1;			FaultProcess();}}
	if( fabs(Inv1.Ib) > Prot.CurrentLim_inv1) 				{ fault_cnt++; if(fault_cnt >=5 ){fault_current = Inv1.Ib;  Fault.SW.bit.I_v = 1;			FaultProcess();}}
	if( fabs(Inv1.Ic) > Prot.CurrentLim_inv1) 				{ fault_cnt++; if(fault_cnt >=5 ){fault_current = Inv1.Ic;  Fault.SW.bit.I_w = 1;			FaultProcess();}}
	if( fabs(Inv1.Wrpm) > 3200)								{ fault_speed = Inv1.Wrpm;  Fault.SW.bit.Speed = 1;			FaultProcess();}
//	if( fabs(Inv1.Idc_sen) > Prot.CurrentDC) 				{ fault_current = Inv1.Idc_sen;Fault.SW.bit.I_dc = 1;									FaultProcess();}
	if(temperture > 80) FaultProcess();
/////////////////////////////////////////////////////////////
////////////////		Angle Generate		 ////////////////
/////////////////////////////////////////////////////////////		

	Thetam_enc = EncoderAngleGenerator();	//Angle from encoder

	
	if(MODE_angle1 == 0){
		Inv1.Thetar = (float)rotor_align_finished * BOUND_PI(Thetam_enc * Inv1.PolePair);
		SpeedObserver(&Inv1, &Obs1);
	}

	else if(MODE_angle1 == 2){
		Inv1.Thetar = BOUND_PI(Theta_IbyF * Inv1.PolePair);
		SpeedObserver(&Inv1, &Obs1);	
	}

 	Inv1.Thetar_SQ = Inv1.Thetar * Inv1.Thetar;
	Inv1.SinTheta = SIN(Inv1.Thetar , Inv1.Thetar_SQ);
	Inv1.CosTheta = COS(Inv1.Thetar_SQ);

/////////////////////////////////////////////////////////////
////////////////		Inertia Estimate	 ////////////////
/////////////////////////////////////////////////////////////

	Inv1.addedThetar =  (float)rotor_align_finished * Thetar_added(Thetam_enc);

///////////////////////////////////////////////////////
////////////////// Current Transformation /////////////
///////////////////////////////////////////////////////

	Inv1.Idss = Inv1.Ia * INV_2_3 - (Inv1.Ib + Inv1.Ic) * INV_3;
	Inv1.Iqss = (Inv1.Ib - Inv1.Ic) * INV_SQRT3;

	Inv1.Idse = Inv1.Idss * Inv1.CosTheta	+ Inv1.Iqss * Inv1.SinTheta;
	Inv1.Iqse = -Inv1.Idss * Inv1.SinTheta + Inv1.Iqss * Inv1.CosTheta;

	DisturbObserver(&Inv1, &Obsd);
	Inv1.Wrm_Err_manual = Inv1.Wrm - Inv1.Wrm_manual_ref;
	Inv1.Thetam_Err_manual = Inv1.Thetam_manual_ref - Inv1.addedThetar;
//////////////////////////////////////////////////////////////////////////////////////
//////////////		Current Control              /////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


	if(SW1.Align_on) RotorAlign();
	else ENC_mode1 = 1;
	if((SW1.CC_on) && (!Fault.SW.all)){
		LED1_ON;
		Switch1On();
		if(SW1.Predict_on)	Is_predict_on = 1;
		if(SW1.PC_on){
			PCProfileGen();						//input : Thetam_ref_set --> output : Thetam_ref, 
			PCPDcontroller();									//input : Thetam_ref --> output : Wrm_ref_set 	
		}
		else{
			Inv1.Thetam_ref = Inv1.addedThetar;
		}
		if(SW1.SC_on){
		 	if(!SW1.PC_on) SCProfileGen();        				//input : Wrm_ref_set --> output : Wrm_ref
		 	else Inv1.Wrm_ref = Inv1.Wrm_ref_set;

		 	sc_cnt++;
			if(SW1.TDOF_on == ON) TDOFcontrol();
			else sc_cnt_max =0;

		 	if(sc_cnt >= sc_cnt_max){
		 		SCPIcontroller();	
		 		sc_cnt	=0;							//input : Wrm_ref     --> output : Iqse_ref
			}
		}
		else {
			Inv1.Wrm_ref_set = 0;
			Inv1.Wrm_ref = 0;
			Inv1.Te_integ = 0;
		}
	
		current_torque = Inv1.Iqse * Inv1.Kt;
		if(sin_on) {
			Inv1.Idse_ref = SINRefGen();
			SIN_record();
		}

		CCPIcontroller();				//by current control reference make current control reference
		

///////////////////////////////////////////////////////
////////////////// Current Transformation /////////////
///////////////////////////////////////////////////////
		/*Inv1.Thetar_predict = Inv1.Thetar+1.5*Inv1.Wr*Ts;
		Inv1.Thetar_SQ_predict = Inv1.Thetar_predict*Inv1.Thetar_predict;
		Inv1.SinTheta_predict = SIN(Inv1.Thetar_predict , Inv1.Thetar_SQ_predict);
		Inv1.CosTheta_predict = COS(Inv1.Thetar_SQ_predict);
		*/
		Inv1.Vdss_ref = Inv1.Vdse_ref * Inv1.CosTheta - Inv1.Vqse_ref * Inv1.SinTheta;
		Inv1.Vqss_ref = Inv1.Vdse_ref * Inv1.SinTheta + Inv1.Vqse_ref * Inv1.CosTheta;
	

		Inv1.Vas_ref = Inv1.Vdss_ref;
		Inv1.Vbs_ref = -0.5 * (Inv1.Vdss_ref - SQRT3 * Inv1.Vqss_ref);
		Inv1.Vcs_ref = -0.5 * (Inv1.Vdss_ref + SQRT3 * Inv1.Vqss_ref);
		

		SVPWM();
		
		
		if(SW1.DeadComp_on)		Deadtime_Compensation();
		
		PreLimiting();

		Deadtime_recover();
		
		Inv1.CompA = (int)((Inv1.Van_ref + half_Vdc) * uTsPerTdsp * INV_Vdc);// + Inv1.CompA_add ;
	   	Inv1.CompB = (int)((Inv1.Vbn_ref + half_Vdc) * uTsPerTdsp * INV_Vdc);// + Inv1.CompB_add ;
		Inv1.CompC = (int)((Inv1.Vcn_ref + half_Vdc) * uTsPerTdsp * INV_Vdc);// + Inv1.CompC_add ;


		EPwm1Regs.CMPA.half.CMPA = Inv1.CompA;
		EPwm2Regs.CMPA.half.CMPA = Inv1.CompB;
		EPwm3Regs.CMPA.half.CMPA = Inv1.CompC;

	//	EPwm1Regs.CMPA.half.CMPA = test_pwm_duty4 * uTsPerTdsp;
	//	EPwm2Regs.CMPA.half.CMPA = test_pwm_duty5 * uTsPerTdsp;
	//	EPwm3Regs.CMPA.half.CMPA = test_pwm_duty6 * uTsPerTdsp;

 	}
	else{
		
		Switch1Off();
		ResetCC1();

		LED1_OFF;
	}
			
		SerialDacOut();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/** 
		@brief		PWM out
	 */
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	EPwm7Regs.ETCLR.bit.INT = 1;	 /*	for Clear Interrupt Flag at PRD */
	EPwm8Regs.ETCLR.bit.INT = 1;	 /* for Clear Interrupt Flag at Zero -->> Double sampling */
	Time_CC = (float)(ccTimer2start - ReadCpuTimer2Counter())*SYSTEM_CLOCK_PRD;	/*	calculating CC loop time */

	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupCC;			/* HW fault & TINT0 || TZ || CC || EasyDSP */



}



float Thetar_added(float thetar){
	float theta_diff = 0.;
	float theta_added = 0.;
	theta_diff = thetar - Inv1.Thetam_prev;
	if (theta_diff > 3.14) theta_diff = 2 * 3.141592 - theta_diff;
	else if (theta_diff < -3.14) theta_diff = 2 * 3.141592 + theta_diff;
	theta_added = Inv1.addedThetar + theta_diff;
	Inv1.Thetam_prev = thetar;
	return theta_added;

}
void PWMCarrierFreqSetting(int freq){

	Fs_set = freq;
	Tsw = 1./Fs_set;
	uTsPerTdsp = (unsigned int)(Tsw*0.5*SYSTEM_CLOCK); // 	Ts/2 -> PWM Peak
	INVuTsPerTdsp = 1./uTsPerTdsp;
	Tsamp = (SW1.Double_sampling_on==ON)? Tsw*0.5 : Tsw;
	Fsamp = 1./Tsamp;

    EPwm1Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
	EPwm2Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
	EPwm3Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
	EPwm4Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
	EPwm5Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
	EPwm6Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
	EPwm7Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
 	EPwm8Regs.TBPRD = uTsPerTdsp; // Period = 2´600 TBCLK counts
	EPwm8Regs.CMPA.half.CMPA = uTsPerTdsp; // Compare A = 400 TBCLK counts

	UpdatePara();

}
void DeadbandSetting(int DeadbandA, int DeadbandB, int DeadbandC){
	EPwm1Regs.DBRED = DeadbandA;
	EPwm1Regs.DBFED = DeadbandA;

	EPwm2Regs.DBRED = DeadbandB;
	EPwm2Regs.DBFED = DeadbandB;
	
	EPwm3Regs.DBRED = DeadbandC;
	EPwm3Regs.DBFED = DeadbandC;

	EPwm4Regs.DBRED = DeadbandA;
	EPwm4Regs.DBFED = DeadbandA;
	
	EPwm5Regs.DBRED = DeadbandB;
	EPwm5Regs.DBFED = DeadbandB;
	
	EPwm6Regs.DBRED = DeadbandC;
	EPwm6Regs.DBFED = DeadbandC;
}
void InitDistObs(DstObs *tmpD, float Wc){
	
	tmpD->Wdis = Wc;
	tmpD->bf_filt = 0.;
	tmpD->af_filt = 0.;
	tmpD->af_filt_prev = 0.;
	tmpD->DisTorque = 0.;
	tmpD->ratio = 1.;
}
void UpdateGainObsd(DstObs *tmpD){
	tmpD->Wdis = 2 * PI * tmpD->Fdis;
}
void DisturbObserver(Motor *tmpM, DstObs *tmpD){

	//	output = (1 - g*Tsamp)*output_prev + g*Tsamp*input;
	//	output_prev = output;

	tmpD->senced = tmpD->Wdis*tmpM->Jm*Inv1.Wrm;
	tmpD->Torque_ref = tmpM->Iqse*tmpM->Kt;
	tmpD->bf_filt = tmpD->senced + tmpD->Torque_ref;
	tmpD->af_filt = (1 - tmpD->Wdis*Tsamp)*tmpD->af_filt_prev + tmpD->Wdis*Tsamp*tmpD->bf_filt;
	tmpD->af_filt_prev = tmpD->af_filt;
	tmpD->DisTorque = tmpD->af_filt - tmpD->senced;

}
void UpdateGainTDOF(TDOF *tmpT){
	
	tmpT->Ki_m = tmpT->Wt_m*tmpT->Kp_m;
	tmpT->Ki_mT = tmpT->Ki_m*Tsamp;

}
void InitTDOF(TDOF *tmpT, float Wt){
	tmpT->Kp_sc = 2 * Inv1.zeta_sc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	tmpT->Ka_sc = 1 / tmpT->Kp_sc;
	tmpT->Ki_sc = Inv1.Wsc*Inv1.Wsc*Inv1.INV_Kt*Inv1.Jm;
	tmpT->Ki_scT = tmpT->Ki_sc*Tsamp;

	tmpT->Kp_m = 0.3;
	tmpT->Wt_m = Wt;
	tmpT->Ki_m = tmpT->Wt_m*tmpT->Kp_m;
	tmpT->Ki_mT = tmpT->Ki_m*Tsamp;

	tmpT->Wrm_Err = 0.;
	tmpT->Wrm_ref = 0.;
	tmpT->Wrm = 0.;
	tmpT->Wrm_est = 0.;
	
	tmpT->Te_ref = 0.;
	tmpT->Te_integ = 0.;
	tmpT->Td_est = 0.;
	tmpT->Td_integ = 0.;
	tmpT->Teff = 0.;
	tmpT->Teff_integ = 0.;

	tmpT->Iqse_ref_set = 0.;
	tmpT->Iqse_ref = 0.;
	tmpT->ratio = 1.;


}
float TDOF_alpha = 1.0;
int temp_cnt = 0;
void TDOFcontrol(){
	temp_cnt ++;
	TDOFV.Wrm_ref = Inv1.Wrm_ref;
	TDOFV.Wrm_Err = TDOFV.Wrm_ref - TDOFV.Wrm_est;
	TDOFV.Te_integ += TDOFV.Ki_scT * (TDOFV.Wrm_Err - TDOFV.Ka_sc *(TDOFV.Te_ref - TDOFV.Iqse_ref_prev * Inv1.Kt));
	TDOFV.Te_ref = TDOFV.Kp_sc * TDOFV.Wrm_Err + TDOFV.Te_integ;

	TDOFV.Iqse_ref_set = TDOFV.Te_ref * Inv1.INV_Kt;
	TDOFV.Iqse_ref = ((TDOFV.Iqse_ref_set > Inv1.Is_rated) ? Inv1.Is_rated : ((TDOFV.Iqse_ref_set < -Inv1.Is_rated) ? -Inv1.Is_rated : TDOFV.Iqse_ref_set));
	TDOFV.Iqse_ref_prev = TDOFV.Iqse_ref;

	TDOFV.Wrm_Err = TDOFV.Wrm_est - Inv1.Wrm;
	TDOFV.Td_integ += TDOFV.Ki_mT * TDOFV.Wrm_Err;
	TDOFV.Td_est = TDOFV.Kp_m * TDOFV.Wrm_Err + TDOFV.Td_integ;

	TDOFV.Teff = TDOFV.Te_ref - TDOFV.Td_est;
	TDOFV.Teff_integ += TDOFV.Teff*Tsamp;
	TDOFV.Wrm_est = TDOFV.Teff_integ * Inv1.InvJm * TDOF_alpha;

}
void RotorAlign(void){
	switch(ENC_mode1){
			case 1:
				SW1.PC_on = OFF;
				SW1.SC_on = OFF;
				rotor_align_finished = 0;
				Inv1.Iqse_ref = 0;
				Inv1.Idse_ref = 4;
				if(Inv1.Idse >= 4 ) ENC_mode1 = 2;
				break;
			case 2:
				time_ENC += Tsamp;
				Inv1.Idse_ref = 4;	
				Inv1.Iqse_ref = 0;			
				if(time_ENC > 2){
					time_ENC = 0;
					ENC_mode1 = 3;
					EQep2Regs.QPOSCNT = 0;
				}
				break;
			case 3:
				Inv1.Idse_ref = 0;
				Inv1.Iqse_ref = 0;					
				if(Inv1.Idse_ref <= 0.){
					Theta_offset2 = 0;					
					time_ENC = 0;
					ENC_mode1 = 4;
					Inv1.Idse_ref = 0;
				}
				break;
			case 4:				
				Inv1.Idse_ref = 0;
				Inv1.Iqse_ref = 0;
				Inv1.Thetam_ref_set= 0.;
				Inv1.Thetam_ref_set_prev = 0.;
				SW1.CC_on = OFF;
				SW1.Align_on = OFF;
				ENC_mode1 = 1;
				rotor_align_finished = 1;
				break;
		}	
}



float EncoderAngleGenerator(void){
	float temp_thetar_enc = 0.;
	EncoderCount2 =  EQep2Regs.QPOSCNT;

	temp_thetar_enc = (float)EncoderCount2 * (float)encoder2.scale;
	return temp_thetar_enc;

}
void PCProfileGen(void){
	switch (pc_mode){
	case 0 :
		if(Inv1.Thetam_ref_set != Inv1.Thetam_ref_set_prev){
			if(SW1.PCCalib_on){
				if(Inv1.Thetam_ref_set > Inv1.Thetam_ref_set_prev){
					Inv1.InitThetar = Inv1.addedThetar;
					pc_mode = 1;
				}
				else if(Inv1.Thetam_ref_set < Inv1.Thetam_ref_set_prev){
					Inv1.InitThetar = Inv1.addedThetar;
					pc_mode = 4;
				}
			}
			else pc_mode = 7;
		}
		pc_time = 0.;
	 	Inv1.Thetam_ref_set_prev = Inv1.Thetam_ref_set;
		break;
	case 1:	//up 2nd order
		Inv1.acc_ref_ff = Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref_ff += Inv1.acc_ref_ff*Tsamp;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		Inv1.Thetam_temp = Inv1.Thetam_ref;
		if (Inv1.Wrm_ref_ff >= Inv1.Wrm_rated) pc_mode = 2;
		else if (Inv1.Thetam_ref >= 0.5 * Inv1.Thetam_ref_set) pc_mode = 3;
		break;
	case 2:	//linear mode
		Inv1.acc_ref_ff = 0.;
		Inv1.Wrm_ref_ff = Inv1.Wrm_rated;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		if (Inv1.Thetam_ref >= (Inv1.Thetam_ref_set - Inv1.Thetam_temp)) pc_mode = 3;
		break;
	case 3:	//down 2nd order
		Inv1.acc_ref_ff = -Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref_ff += Inv1.acc_ref_ff*Tsamp;
		if (Inv1.Wrm_ref_ff <= 0) Inv1.Wrm_ref_ff = 0;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		if ((Inv1.Thetam_ref >= Inv1.Thetam_ref_set) || (Inv1.Wrm_ref_ff <= 0)){
			pc_mode = 7;
		}
		break;
	case 4:	//up 2nd order
		Inv1.acc_ref_ff = -Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref_ff += Inv1.acc_ref_ff*Tsamp;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		Inv1.Thetam_temp = Inv1.Thetam_ref;
		if (Inv1.Wrm_ref_ff <= -Inv1.Wrm_rated) pc_mode = 5;
		else if ((Inv1.InitThetar - Inv1.Thetam_ref) >= 0.5* (Inv1.InitThetar - Inv1.Thetam_ref_set)) pc_mode = 6;

		break;
	case 5:	//linear mode
		Inv1.acc_ref_ff = 0.;
		Inv1.Wrm_ref_ff = -Inv1.Wrm_rated;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		if (Inv1.Thetam_ref <= (Inv1.Thetam_ref_set + Inv1.InitThetar - Inv1.Thetam_temp)) pc_mode = 6;
		break;
	case 6:	//down 2nd order
		Inv1.acc_ref_ff = Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref_ff += Inv1.acc_ref_ff*Tsamp;
		if (Inv1.Wrm_ref_ff >= 0) Inv1.Wrm_ref_ff = 0;
		Inv1.Thetam_ref += Inv1.Wrm_ref_ff*Tsamp;
		if ((Inv1.Thetam_ref <= Inv1.Thetam_ref_set) || (Inv1.Wrm_ref_ff >= 0))		pc_mode = 7;
		break;
	case 7:
		pc_time += Tsamp;
		Inv1.acc_ref_ff = 0;
		Inv1.Wrm_ref_ff = 0;
		Inv1.Thetam_ref = Inv1.Thetam_ref_set;
		if(pc_time >= 0.2) pc_mode = 0;
		break;
	}
	pc_mode_float = pc_mode;
}

void SCProfileGen(void){

	switch(sc_mode){
	case 0 : 
		if(Inv1.Wrm_ref_set != Inv1.Wrm_ref_set_prev){
			if(SW1.SCCalib_on){
				if(Inv1.Wrm_ref_set > Inv1.Wrm_ref_set_prev){
					sc_mode = 1;
				}
				else if(Inv1.Wrm_ref_set < Inv1.Wrm_ref_set_prev){
					sc_mode = 2;
				}
			}
			else sc_mode = 3;
		}

		sc_time = 0.;
		Is_predict_on = 0;
		Inv1.Wrm_ref_set_prev = Inv1.Wrm_ref_set;
		break;
	case 1 : 
		Inv1.acc_ref_ff = Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref += Inv1.acc_ref_ff*Tsamp;
		if(Inv1.Wrm_ref >= Inv1.Wrm_ref_set) {
			sc_mode = 3;
			if(SW1.Predict_on)	Is_predict_on = 1;
		}
	break;
	case 2 : 
		Inv1.acc_ref_ff = -Inv1.Is_rated*Inv1.Kt*Inv1.InvJm;
		Inv1.Wrm_ref += Inv1.acc_ref_ff*Tsamp;
		if(Inv1.Wrm_ref <= Inv1.Wrm_ref_set) {
			sc_mode = 3;
			if(SW1.Predict_on)	Is_predict_on = 1;
		}
		break;
	case 3 : 
		Inv1.Wrm_ref = Inv1.Wrm_ref_set;
		Inv1.acc_ref_ff = 0.;
		sc_time +=Tsamp;
		if(sc_time >= 0.1) sc_mode = 0.;
	break;
	}
	sc_mode_float = sc_mode;
	}
void PCPDcontroller(void){
	Inv1.Thetam_Err_true = Inv1.Thetam_ref_set - Inv1.addedThetar;
	Inv1.Thetam_Err = Inv1.Thetam_ref - Inv1.addedThetar;
	Inv1.Thetam_Err_diff = (Inv1.Thetam_Err - Inv1.Thetam_Err_prev);

	Inv1.Wrm_ref_set = Inv1.Wrm_ref_ff + Inv1.Kp_pc * Inv1.Thetam_Err + Inv1.Kd_pcT * Inv1.Thetam_Err_diff;
	Inv1.Wrm_ref_set = ((Inv1.Wrm_ref_set >= Inv1.Wrm_rated) ? Inv1.Wrm_rated : ((Inv1.Wrm_ref_set <= -1.0*Inv1.Wrm_rated) ? -1.0*Inv1.Wrm_rated : Inv1.Wrm_ref_set));

	Inv1.Thetam_Err_prev = Inv1.Thetam_Err;
}
float temp_value = 0.;

void SCPIcontroller(void){

	Inv1.Wrm_Err = Inv1.Wrm_ref - Inv1.Wrm;
	temp_value = Inv1.Ka_sc * (Inv1.Te_ref - Inv1.Iqse_ref_unmodified * Inv1.Kt);
	Inv1.Te_integ += Inv1.Wrm_Err - temp_value;
	Inv1.Te_ref = Inv1.acc_ref_ff*Inv1.Jm + Inv1.Kp_sc * Inv1.Wrm_Err + Inv1.Ki_scT * Inv1.Te_integ;
	if(SW1.TDOF_on == ON) Inv1.Te_ref += TDOFV.Te_ref + TDOFV.ratio*TDOFV.Td_est; //TDOF for self resonance
	Inv1.Iqse_ref_set = Inv1.Te_ref * Inv1.INV_Kt;
	// Inv1.Iqse_ref_set = (Inv1.Te_ref - Obsd.ratio * Obsd.DisTorque)* Inv1.INV_Kt;

	Inv1.Iqse_ref_unmodified = ((Inv1.Iqse_ref_set > Inv1.Is_rated) ? Inv1.Is_rated : ((Inv1.Iqse_ref_set < -Inv1.Is_rated) ? -Inv1.Is_rated : Inv1.Iqse_ref_set));
	if(SW1.DOB_on == ON) Inv1.Iqse_ref =  Inv1.Iqse_ref_unmodified - (Obsd.ratio * Obsd.DisTorque)* Inv1.INV_Kt; //DOB for load resonance
	else Inv1.Iqse_ref = Inv1.Iqse_ref_unmodified;
	Inv1.Idse_ref = 0;
}


void CCPIcontroller(void){

	Inv1.Idse_future = (1 - Inv1.Rs * Inv1.INV_Ls*Tsamp)*Inv1.Idse + (Inv1.Wr*Tsamp)*Inv1.Iqse + (Tsamp * Inv1.INV_Ls)*Inv1.Vdse_ref_prev;
	Inv1.Iqse_future = -(Inv1.Wr*Tsamp)*Inv1.Idse + (1 - Inv1.Rs *Inv1.INV_Ls*Tsamp)*Inv1.Iqse + (Tsamp * Inv1.INV_Ls)*Inv1.Vqse_ref_prev - Inv1.Wr*Inv1.Ke * Inv1.INV_Ls *Tsamp;
	
	if(Is_predict_on) {
		Inv1.Err_Idse = Inv1.Idse_ref - Inv1.Idse_future;
		Inv1.Err_Iqse = Inv1.Iqse_ref - Inv1.Iqse_future;
	}
	else{
		Inv1.Err_Idse = Inv1.Idse_ref - Inv1.Idse;
		Inv1.Err_Iqse = Inv1.Iqse_ref - Inv1.Iqse;
	}
	Inv1.Idse_integ +=  Inv1.Kid_T *(Inv1.Err_Idse - Inv1.Kad * Inv1.Vdse_anti);
	Inv1.Iqse_integ +=  Inv1.Kiq_T *(Inv1.Err_Iqse - Inv1.Kaq * Inv1.Vqse_anti);

	Inv1.Vdse_ref_ff = -Inv1.Iqse * Inv1.Lqs * Inv1.Wr;
	Inv1.Vqse_ref_ff = Inv1.Idse * Inv1.Lds * Inv1.Wr + Inv1.Wr * Inv1.Ke;

	Inv1.Vdse_ref = Inv1.Kpd * Inv1.Err_Idse + Inv1.Idse_integ + Inv1.Vdse_ref_ff;
	Inv1.Vqse_ref = Inv1.Kpq * Inv1.Err_Iqse + Inv1.Iqse_integ + Inv1.Vqse_ref_ff;

	Inv1.Idse_ref_old = Inv1.Idse_ref;
	Inv1.Iqse_ref_old = Inv1.Iqse_ref;
}

void SVPWM(void){


		if(Inv1.Vas_ref > Inv1.Vbs_ref){
			Inv1.Vmax = Inv1.Vas_ref; 
			Inv1.Vmin = Inv1.Vbs_ref;
		}
		else{
			Inv1.Vmax = Inv1.Vbs_ref; 
			Inv1.Vmin = Inv1.Vas_ref;
		}
		if(Inv1.Vcs_ref > Inv1.Vmax)
		{
			Inv1.Vmax = Inv1.Vcs_ref;
		}
		if(Inv1.Vcs_ref < Inv1.Vmin)
		{
			Inv1.Vmin = Inv1.Vcs_ref;
		}
		Inv1.Vsn= -0.5 * (Inv1.Vmax + Inv1.Vmin);
	    	
		Inv1.Van_ref_set = Inv1.Vas_ref + Inv1.Vsn;
		Inv1.Vbn_ref_set = Inv1.Vbs_ref + Inv1.Vsn;
		Inv1.Vcn_ref_set = Inv1.Vcs_ref + Inv1.Vsn;
}


void Deadtime_Compensation(){
	Inv1.Ia_change_DB = temp_deadA * INVuTsPerTdsp * Vdc; // DTC
	if(fabs(Inv1.Ia)< Inv1.slope_start_I) { // ZCC
		Inv1.Ia_change_DB = Inv1.Ia_change_DB * (1 - pow(1-fabs(Inv1.Ia)*Inv1.INV_slope_start_I,3));
	}
	Inv1.Ia_change_DB = (Inv1.Ia > 0) ? Inv1.Ia_change_DB : -Inv1.Ia_change_DB; // direction
	
	Inv1.Ib_change_DB = temp_deadB * INVuTsPerTdsp * Vdc; // DTC
	if(fabs(Inv1.Ib)< Inv1.slope_start_I) { // ZCC
		Inv1.Ib_change_DB = Inv1.Ib_change_DB * (1 - pow(1-fabs(Inv1.Ib)*Inv1.INV_slope_start_I,3));
	}
	Inv1.Ib_change_DB = (Inv1.Ib > 0) ? Inv1.Ib_change_DB : -Inv1.Ib_change_DB; // direction
	
	Inv1.Ic_change_DB = temp_deadC * INVuTsPerTdsp * Vdc; // DTC
	if(fabs(Inv1.Ic)< Inv1.slope_start_I) { // ZCC
		Inv1.Ic_change_DB = Inv1.Ic_change_DB * (1 - pow(1-fabs(Inv1.Ic)*Inv1.INV_slope_start_I,3));
	}
	Inv1.Ic_change_DB = (Inv1.Ic > 0) ? Inv1.Ic_change_DB : -Inv1.Ic_change_DB; // direction

		Inv1.Van_ref_set += Inv1.Ia_change_DB;
		Inv1.Vbn_ref_set += Inv1.Ib_change_DB;
		Inv1.Vcn_ref_set += Inv1.Ic_change_DB;
}

void PreLimiting(void){

		Inv1.Van_ref = (Inv1.Van_ref_set > half_Vdc ) ? half_Vdc  : ((Inv1.Van_ref_set < -half_Vdc) ? -half_Vdc : Inv1.Van_ref_set);		// < ? a:b > true->a , false ->b
		Inv1.Vbn_ref = (Inv1.Vbn_ref_set > half_Vdc ) ? half_Vdc  : ((Inv1.Vbn_ref_set < -half_Vdc) ? -half_Vdc : Inv1.Vbn_ref_set);
		Inv1.Vcn_ref = (Inv1.Vcn_ref_set > half_Vdc ) ? half_Vdc  : ((Inv1.Vcn_ref_set < -half_Vdc) ? -half_Vdc : Inv1.Vcn_ref_set);	

		Inv1.Van_diff = Inv1.Van_ref_set - Inv1.Van_ref;
		Inv1.Vbn_diff = Inv1.Vbn_ref_set - Inv1.Vbn_ref;
		Inv1.Vcn_diff = Inv1.Vcn_ref_set - Inv1.Vcn_ref;

		Inv1.Vdss_anti = Inv1.Van_diff * INV_3 * 2 - Inv1.Vbn_diff * INV_3 - Inv1.Vcn_diff * INV_3;
		Inv1.Vqss_anti = Inv1.Vbn_diff * SQRT3 * INV_3 - Inv1.Vcn_diff * SQRT3 * INV_3;

		Inv1.Vdse_anti = Inv1.Vdss_anti * 	Inv1.CosTheta + Inv1.Vqss_anti * Inv1.SinTheta;
		Inv1.Vqse_anti = -Inv1.Vdss_anti * 	Inv1.SinTheta + Inv1.Vqss_anti * Inv1.CosTheta;

}
void Deadtime_recover(void){
		Inv1.Van_ref_dt = Inv1.Van_ref - Inv1.Ia_change_DB;
		Inv1.Vbn_ref_dt = Inv1.Vbn_ref - Inv1.Ib_change_DB;
		Inv1.Vcn_ref_dt = Inv1.Vcn_ref - Inv1.Ic_change_DB;

		Inv1.Vdss_limited = Inv1.Van_ref_dt * INV_3 * 2 - Inv1.Vbn_ref_dt * INV_3 - Inv1.Vcn_ref_dt * INV_3;
		Inv1.Vqss_limited = Inv1.Vbn_ref_dt * SQRT3 * INV_3 - Inv1.Vcn_ref_dt * SQRT3 * INV_3;
		
		Inv1.Vdse_limited = Inv1.Vdss_limited * Inv1.CosTheta + Inv1.Vqss_limited * Inv1.SinTheta;
		Inv1.Vqse_limited = -Inv1.Vdss_limited * Inv1.SinTheta + Inv1.Vqss_limited * Inv1.CosTheta;

			
		Inv1.Vdse_ref_prev = Inv1.Vdse_limited;
		Inv1.Vqse_ref_prev = Inv1.Vqse_limited;

}
void SpeedObserver (Motor *tmpM, SpdObs *tmpO){

	tmpO->Err_Theta = BOUND_PI(tmpM->Thetar - tmpO->Thetar_est) * tmpM->InvPolePair ;
	tmpO->Err_Theta_integ += Tsamp * tmpO->Err_Theta;

	tmpO->Tl_est = tmpO->Ki_so * tmpO->Err_Theta_integ;
	tmpM->Te_real = tmpM->Kt * tmpM->Iqse_ref;

	tmpO->Acc_integ = tmpM->Te_real + tmpO->Kp_so * tmpO->Err_Theta + tmpO->Tl_est - tmpM->Bm*tmpO->Wrm_est;

	tmpO->Wrm_est += Tsamp * (tmpO->Acc_integ * tmpM->InvJm);
	tmpO->Wrm_est_d = tmpO->Kd_so * tmpO->Err_Theta;
	
	tmpO->Thetar_est = BOUND_PI(tmpO->Thetar_est + Tsamp*(tmpO->Wrm_est + tmpO->Wrm_est_d)*tmpM->PolePair);
	
	tmpM->Wrm = tmpO->Wrm_est + tmpO->Wrm_est_d;
	tmpM->Wr = tmpM->Wrm * tmpM->PolePair;
	tmpM->Wrpm = Rm2Rpm * tmpM->Wrm;


}
void InitObs(SpdObs *tmpO, float Wc, float Jm){

	tmpO->Theta_est = 0., tmpO->Err_Theta = 0., tmpO->Err_Theta_integ = 0.;
	tmpO->Te_est = 0., tmpO->Te_est_old = 0., tmpO->Tl_est = 0.;
	tmpO->Acc_integ = 0.;
	tmpO->Wr_est = 0.;
	tmpO->Wrm_est_d = 0.;
	tmpO->Wrm_est = 0., tmpO->Wrpm_est = 0.;
	tmpO->Wso = Wc;
	tmpO->Wc_filter = 300.;
	tmpO->alpha_ff = 0.;
	tmpO->Theta_est_d = 0.;
	
	tmpO->Tl_est_d = 0.;
	tmpO->Thetar_est = 0.;


}
float temp_paraP = 1;
float temp_paraI = 1.;

float beta;
float l1 = 0, l2 = 0, l3 = 0;

void UpdateGainObs(Motor *tmpM, SpdObs *tmpO){
	tmpO->Wso = 2*PI*tmpO->Fso;
	beta = -1.0*tmpO->Wso;

	l1 = -3.0*beta;
	l2 = 3.0*beta*beta;
	l3 = beta*beta*beta*tmpM->Jm;

	tmpO->Kd_so = l1;
	tmpO->Kp_so = tmpM->Jm*l2;
	tmpO->Ki_so = -1.0*l3;
	
}


void Switch1Off(void)	
{
	EPwm1Regs.AQSFRC.bit.OTSFA = 1;
	EPwm1Regs.AQSFRC.bit.OTSFB = 1;
	EPwm2Regs.AQSFRC.bit.OTSFA = 1;
	EPwm2Regs.AQSFRC.bit.OTSFB = 1;
	EPwm3Regs.AQSFRC.bit.OTSFA = 1;
	EPwm3Regs.AQSFRC.bit.OTSFB = 1;

	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;

	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;

	EPwm1Regs.CMPA.half.CMPA = 0;
	EPwm2Regs.CMPA.half.CMPA = 0;
	EPwm3Regs.CMPA.half.CMPA = 0;
}	

void Switch1On(void){
	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
}

void ResetCC1(void){
	
	Idse1_ref_set = 0;
	Iqse1_ref_set = 0;
	Inv1.Idse_ref_set = 0.;	Inv1.Iqse_ref_set = 0.;
 	Inv1.Idse_ref = 0.;		Inv1.Iqse_ref = 0.;	
	Inv1.Iqse_ref_unmodified = 0.;

	current_test =0.; 			theta=90;

//	Inv1.Ia=0.;  Inv1.Ib=0.;   Inv1.Ic=0.; Inv1.Idc_sen=0.; 
	
//	Inv1.SinTheta=0.;	Inv1.CosTheta=1.;

	Inv1.Err_Idse=0.;			Inv1.Err_Iqse=0.;
	Inv1.Idse_integ=0.;			Inv1.Iqse_integ=0.;
	Inv1.Vdse=0.;				Inv1.Vqse=0.;
	Inv1.Vdse_ref=0.;			Inv1.Vqse_ref=0.;
	Inv1.Vdse_ref_ff=0.;		Inv1.Vqse_ref_ff=0.;
	Inv1.Vdse_ref_fb=0.;		Inv1.Vqse_ref_fb=0.;
	Inv1.Vdse_ref=0.;			Inv1.Vqse_ref=0.;
	Inv1.Vdss_ref=0.;			Inv1.Vqss_ref=0.;
	Inv1.Vdss_ref_old=0.;		Inv1.Vqss_ref_old=0.;
	Inv1.Vas_ref=0.;			Inv1.Vbs_ref=0.;		Inv1.Vcs_ref=0.;
	Inv1.Vas=0.;				Inv1.Vbs=0.;			Inv1.Vcs=0.;
	Inv1.Vdss=0.;				Inv1.Vqss=0.;

	Inv1.Wrm_Err = 0.;
	Inv1.Te_integ = 0.;
	Inv1.Te_ref = 0.;

	Inv1.Van_ref_set=0.;		Inv1.Vbn_ref_set=0.;	Inv1.Vcn_ref_set=0.;
	Inv1.Vdse_ref_set=0.;			Inv1.Vqse_ref_set=0.;
	Inv1.Vdss_ref_set=0.;			Inv1.Vqss_ref_set=0.;
	Inv1.Vdqse_ref = 0.;

	Inv1.Vdse_ref_set_ave=0.,	Inv1.Vqse_ref_set_ave=0.;
	Inv1.Vdse_ref_lim=0.,	Inv1.Vqse_ref_lim=0.; 

	Inv1.Vdse_ref_set_old=0.,	Inv1.Vqse_ref_set_old=0.; 

	Inv1.Vdss_ref_set_old=0.,	Inv1.Vqss_ref_set_old=0.; 
	Inv1.Vas_ref_set_old=0.,	Inv1.Vbs_ref_set_old=0., Inv1.Vcs_ref_set_old=0.;; 

	Inv1.Vdse_anti=0.,			Inv1.Vqse_anti=0.;
	Inv1.CompA = 0.;			Inv1.CompB = 0.;			Inv1.CompC = 0.;
	Inv1.CompA_add = 0.;		Inv1.CompB_add = 0.;		Inv1.CompC_add = 0.;
	Inv1.CompA_ZCC = 0.;		Inv1.CompB_ZCC = 0.;		Inv1.CompC_ZCC = 0.;
	Flag1_DTS=0.; 				Flag1_ZCC=0.; Flag_SL1=0;

	Inv1.Van_ref = 0.;			Inv1.Vbn_ref = 0.;			Inv1.Vcn_ref = 0.;
	Inv1.Vmax = 0.;	Inv1.Vmin = 0.;	Inv1.Vsn = 0.;
	d_axis1=0;  d_aix_current1=0;  test_rpm_set1=0;  test_f1=0; test_theta1=0;
	Inv1.Wrpm=0; 

	Inv1_SC.Wrm=0; Inv1_SC.Wrm_ref=0; Inv1_SC.Wrm_ref_old=0;
	Inv1_SC.Wrpm_ref_set=0; Inv1_SC.Te_ref_set=0;
	Inv1_SC.Err_Wrm=0; Inv1_SC.Iqse_ref_set=0;
	MODE_angle1=0; 
	Flag_Injec=0; Mode_Injec=0;

	//flux weakening
	Inv1.Err_flux = 0.;
	Inv1.flux_integ = 0.;
	Inv1.flux_anti = 0.;
	Inv1.flux_ref =0.;
	Inv1.flux_ref_lim = 0.;
	Inv1.flux_ref_min = 0.;

	//InitTDOF(&TDOFV, 2 * PI*0.005);
	
	SW1.PC_on = OFF;
	SW1.SC_on = OFF;
	SW1.CC_on = OFF;
	SW1.Align_on = OFF;

}

float SINRefGen(void){
	float temp_sin_ref = 0.;
	if(!sin_on) temp_theta = 0;
	temp_theta += 2*PI*sin_freq*Tsamp;
	temp_theta =  BOUND_PI(temp_theta);
	temp_sin_ref =  sin_mag * SIN(temp_theta,temp_theta*temp_theta);
	return temp_sin_ref;		
}
void SIN_record(){
	Idse_ref_array[arr_order][arr_num] = Inv1.Idse_ref;
	Idse_sen_array[arr_order][arr_num] = Inv1.Idse;
	arr_num++;
	record_finish = 0;
	if(arr_num > max_arr_num) {
		arr_order++;
		arr_num = 0;
		if(arr_order > max_arr_order) {
	 		record_finish = 1;
			sin_on = 0;
			arr_num = 0;
			arr_order = 0;
			Inv1.Idse_ref = 0.;
		}

	}
}

void faultTz(void)
{
	EPwm1Regs.AQSFRC.bit.OTSFA = 1;
	EPwm1Regs.AQSFRC.bit.OTSFB = 1;
	EPwm2Regs.AQSFRC.bit.OTSFA = 1;
	EPwm2Regs.AQSFRC.bit.OTSFB = 1;
	EPwm3Regs.AQSFRC.bit.OTSFA = 1;
	EPwm3Regs.AQSFRC.bit.OTSFB = 1;
	EPwm4Regs.AQSFRC.bit.OTSFA = 1;
	EPwm4Regs.AQSFRC.bit.OTSFB = 1;
	EPwm5Regs.AQSFRC.bit.OTSFA = 1;
	EPwm5Regs.AQSFRC.bit.OTSFB = 1;
	EPwm6Regs.AQSFRC.bit.OTSFA = 1;
	EPwm6Regs.AQSFRC.bit.OTSFB = 1;

	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm4Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm5Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;
	EPwm6Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;

	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm4Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm5Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm5Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm6Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm6Regs.AQCTLA.bit.CAD = AQ_CLEAR;
   // LED2_ON;
    

	EPwm1Regs.CMPA.half.CMPA = 0;
	EPwm2Regs.CMPA.half.CMPA = 0;
	EPwm3Regs.CMPA.half.CMPA = 0;
	EPwm4Regs.CMPA.half.CMPA = 0;
	EPwm5Regs.CMPA.half.CMPA = 0;
	EPwm6Regs.CMPA.half.CMPA = 0;
//    test_pwm=0;
	SW1.CC_on = OFF;
	SW1.PC_on = OFF;
	SW1.SC_on = OFF;
	SW1.Align_on = OFF;
}


void ResetTZCC(void)
{   
	EPwm1Regs.TZCLR.bit.CBC = 1;
	EPwm1Regs.TZCLR.bit.INT = 1;
    EPwm1Regs.TZCLR.bit.OST = 1;

	EPwm2Regs.TZCLR.bit.CBC = 1;
	EPwm2Regs.TZCLR.bit.INT = 1;
	EPwm2Regs.TZCLR.bit.OST = 1;


	EPwm3Regs.TZCLR.bit.CBC = 1;
	EPwm3Regs.TZCLR.bit.INT = 1;
	EPwm3Regs.TZCLR.bit.OST = 1;


	EPwm4Regs.TZCLR.bit.CBC = 1;
	EPwm4Regs.TZCLR.bit.INT = 1;
    EPwm4Regs.TZCLR.bit.OST = 1;
	
	EPwm5Regs.TZCLR.bit.CBC = 1;
	EPwm5Regs.TZCLR.bit.INT = 1;
    EPwm5Regs.TZCLR.bit.OST = 1;

	EPwm6Regs.TZCLR.bit.CBC = 1;
	EPwm6Regs.TZCLR.bit.INT = 1;
    EPwm6Regs.TZCLR.bit.OST = 1;
}






