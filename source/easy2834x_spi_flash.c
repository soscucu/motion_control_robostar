/***************************************************************
    spi_flash.c
	2010.01.15 : 	first release
	by Daewoong Chung
****************************************************************/
#include "DSP2834x_Device.h"

/////////////////////////////////////////////////////////////////
// NOTICE : Please change below CPU_CLK, LSP_CLK
//          according to your system
//          Don't change any other variables and source
/////////////////////////////////////////////////////////////////
#define CPU_CLK		300000000L				// 300MHz
//#define CPU_CLK		200000000L			// 200MHz
//#define CPU_CLK		150000000L			// 150MHz
//#define CPU_CLK		120000000L			// 120MHz
//#define	LSP_CLK		(CPU_CLK/4)				// reset default

#define	LSP_CLK		(CPU_CLK/2)				// reset default

unsigned int ezDSP_uSPIFlashInitError = 0;
void easyDSP_SPI_Flashrom_Init(void)
{
	EALLOW;
	
	// clock enable
	SysCtrlRegs.PCLKCR0.bit.SPIAENCLK = 1;   // SPI-A
	
	/* Enable internal pull-up for the selected pins */
	// Pull-ups can be enabled or disabled by the user.
	// This will enable the pullups for the specified pins.
    GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;   // Enable pull-up on GPIO16 (SPISIMOA)
    GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0;   // Enable pull-up on GPIO17 (SPISOMIA)
    GpioCtrlRegs.GPAPUD.bit.GPIO18 = 0;   // Enable pull-up on GPIO18 (SPICLKA)
    GpioCtrlRegs.GPAPUD.bit.GPIO19 = 0;   // Enable pull-up on GPIO19 (SPISTEA)

	/* Set qualification for selected pins to asynch only */
	// This will select asynch (no qualification) for the selected pins.
    GpioCtrlRegs.GPAQSEL2.bit.GPIO16 = 3; // Asynch input GPIO16 (SPISIMOA)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 3; // Asynch input GPIO17 (SPISOMIA)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO18 = 3; // Asynch input GPIO18 (SPICLKA)

	/* Configure SPI-A pins using GPIO regs*/
	// This specifies which of the possible GPIO pins will be SPI functional pins.
    GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 1; // Configure GPIO16 as SPISIMOA
    GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 1; // Configure GPIO17 as SPISOMIA
    GpioCtrlRegs.GPAMUX2.bit.GPIO18 = 1; // Configure GPIO18 as SPICLKA

    // IOPORT as output pin instead of SPISTE
    GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 0;
    GpioCtrlRegs.GPADIR.bit.GPIO19 = 1;
    
    // Chip enable - high
    GpioDataRegs.GPASET.bit.GPIO19 = 1;

    EDIS;
  
	// Initialize SPI FIFO registers
    SpiaRegs.SPIFFTX.all=0xE040;
    SpiaRegs.SPIFFRX.all=0x605f;				// mine
    SpiaRegs.SPIFFCT.all=0x0;					// no delay necessary since SCI comm makes it automatically

	// Init SPI
	SpiaRegs.SPICCR.all =0x0047;	            // Reset on, clk polarity=1, 8-bit char bits  
	SpiaRegs.SPICTL.all =0x0006;    		    // Enable master mode, normal phase,
                                                // enable talk, and SPI int disabled.

	// SPI Baudrate = 1M around
	// No need for fast SPI baudrate by considering slow byte-to-byte SCI comm (max,230400bps)
	SpiaRegs.SPIBRR = (int)(((float)LSP_CLK/(1000000L) - 1) + 0.5);
	//SpiaRegs.SPIBRR =0x0;						// quick
	//SpiaRegs.SPIBRR =0x007F;					// slow

    SpiaRegs.SPICCR.all =0x00C7;		        // Relinquish SPI from Reset   
    SpiaRegs.SPIPRI.bit.FREE = 1;               // Set so breakpoints don't disturb xmission

	// confirm status
	if(SpiaRegs.SPIFFTX.bit.TXFFST != 0) ezDSP_uSPIFlashInitError++;
	if(SpiaRegs.SPIFFRX.bit.RXFFST != 0) ezDSP_uSPIFlashInitError++;
}
