/***************************************************************
	COMP.C
	
	programmed by Yo-chan Son, Oct 2000.
	copyright (c) 1991, 2000 by EEPEL, SNU, SEOUL, KOREA
	All Rights Reserved.
****************************************************************/
#include <math.h>
#include "comp.h"

const int atanSize = ATAN_TABLE_SIZE;
float *p_atanTable[ATAN_TABLE_SIZE];
float xxx=0;


float SIGN(const float x)
{
	if (x < 0)	return(-1);
	else		return(1);
}

int atanTableInit(void)
{
int i;
//	test_e=(int *)malloc(sizeof(int)*4);
	
//	*(int*)(test_e+1)=4;
//	xx=*(int*)(test_e+1);
//	p_atanTable = (float *)malloc(sizeof(float)*(atanSize + 1));
//	p_atanTable = new float[atanSize + 1];
		
	if(p_atanTable == 0)	return(0);
	else
	{
		*(float *)p_atanTable = 0.;
		for(i = 0; i <= atanSize; i++)
		*(float *)(p_atanTable+i) = atan((float)i/(float)atanSize);
	}
                       
    return((int)p_atanTable);

}

float atan2Table(float y, float x)
{
float result; 
	x+=1.e-3;

	switch((((y>x)<<2)|((y>-x)<<1)|((y*x)>0.))){\
	case 0: result=-0.5*PI+	(*(float *)(p_atanTable + (int)((-x/y)*(float)atanSize + 0.5)));break;\
	case 1: result=-0.5*PI-	(*(float *)(p_atanTable + (int)((x/y)*(float)atanSize + 0.5)));break;\
	case 2: result=-(*(float *)(p_atanTable + (int)((-y/x)*(float)atanSize + 0.5)));break;\
	case 3: result=	(*(float *)(p_atanTable + (int)((y/x)*(float)atanSize + 0.5)));break;\
	case 4: result=PI-	(*(float *)(p_atanTable + (int)((-y/x)*(float)atanSize + 0.5)));break;\
	case 5: result=-PI+	(*(float *)(p_atanTable + (int)((y/x)*(float)atanSize + 0.5)));break;\
	case 6: result= 0.5*PI+	(*(float *)(p_atanTable + (int)((-x/y)*(float)atanSize + 0.5)));break;\
	default: result= 0.5*PI-	(*(float *)(p_atanTable + (int)((x/y)*(float)atanSize + 0.5)));}


	return(result);
}		
