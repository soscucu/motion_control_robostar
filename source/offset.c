/*****************************************************************************
    OFFSET.C
******************************************************************************/
#include <stdlib.h>
#include <math.h>
#include "DSP2834x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2834x_Examples.h"   // DSP2833x Examples Include File
#include "MPLD.h"
#include "da.h"
#include "comp.h"
#include "variable.h"

extern interrupt void cc(void);	/* for CC*/



int offset_v = 0;

interrupt void offset(void)
{
	int i=0;


	// Conversion Delay
	SerialDacOut();	// about 1us

	offsetLoopCnt++;
	if (offset_v == 0 )
	{
		if (offsetLoopCnt >= 100 )
		{
			offset_v = 1;
			offsetLoopCnt = 0 ;
		}
	}

	else
	{

		delaycc(Tdelay);	/* 측정시간 확보를 위해 Tdelay만큼 측정시간을 미룸 */

		AD_RD0= 0x100;
		AD_RD1= 0x100;
		AD_RD2= 0x100;
  
  		delaycc(0.005e-6);
	
		ADC0_SOC_START;
		ADC1_SOC_START;
		ADC2_SOC_START;	
	
		delaycc(0.005e-6);
		ADC0_SOC_END;
		ADC1_SOC_END;
		ADC2_SOC_END;

		while(ADC0_BUSY);
	//	delaycc(0.005e-6);	
		OffsetAin[0] += (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT
		OffsetAin[1] += (AD_RD0 - 0x0800) & 0x0FFF;		

		while(ADC1_BUSY);
	//	delaycc(0.005e-6);	

		OffsetAin[2] += (AD_RD1 - 0x0800) & 0x0FFF;			//		AD_VOUT 
		OffsetAin[3] += (AD_RD1) & 0x0FFF;			

		while(ADC2_BUSY);
	//	delaycc(0.005e-6);

		OffsetAin[4] += (AD_RD2 - 0x0800) & 0x0FFF;			//		AD_VDC
		OffsetAin[5] += (AD_RD2 - 0x0800) & 0x0FFF;


 	  	AD_RD0 = 0x500;
		AD_RD1 = 0x500;
		AD_RD2 = 0x500;

		delaycc(0.005e-6);

 	  	ADC0_SOC_START;
		ADC1_SOC_START;
		ADC2_SOC_START;
		delaycc(0.005e-6);
		ADC0_SOC_END;
		ADC1_SOC_END;
		ADC2_SOC_END;

		while(ADC0_BUSY);

		OffsetAin[6] += (AD_RD0 - 0x0800) & 0x0FFF;			//		Vvn
		OffsetAin[7] += (AD_RD0 - 0x0800) & 0x0FFF;			//		Dummy

		while(ADC1_BUSY);

		OffsetAin[8] += (AD_RD1 - 0x0800) & 0x0FFF;			//		Vwn
		OffsetAin[9] += (AD_RD1 - 0x0800) & 0x0FFF;			//		Dummy

		while(ADC2_BUSY);

		OffsetAin[10] += (AD_RD2 - 0x0800) & 0x0FFF;			//		Dummy
		OffsetAin[11] += (AD_RD2 - 0x0800) & 0x0FFF;			//		Dummy

	   	AD_RD0 = 0x900;
		AD_RD1 = 0x900;
	//	AD_RD2 = 0x900;
	
		delaycc(0.005e-6);

	    ADC0_SOC_START;
		ADC1_SOC_START;
		delaycc(0.005e-6);
		ADC0_SOC_END;
		ADC1_SOC_END;


	   	while(ADC0_BUSY);
	
		OffsetAin[12] += (AD_RD0 - 0x0800) & 0x0FFF;			//		Vvn
		OffsetAin[13] += (AD_RD0 - 0x0800) & 0x0FFF;			//		Dummy
		
		while(ADC1_BUSY);
	
		OffsetAin[14] += (AD_RD1 - 0x0800) & 0x0FFF;			//		Vvn
		OffsetAin[15] += (AD_RD1 - 0x0800) & 0x0FFF;			//		Dummy

	}

// ADC Offset setting

	// Offset value calculation
	if(offsetLoopCnt >= offsetMaxCnt)
	{
		// Calc. offset
		for (i=0; i<16; i++)
		{
			OffsetAin[i] = OffsetAin[i] / (float)offsetMaxCnt;
		}

		EALLOW;
		PieVectTable.EPWM7_INT = &cc;
		PieVectTable.EPWM8_INT = &cc;
		EDIS;
	}

	EPwm7Regs.ETCLR.bit.INT = 1; 					//	for interrupt at zero
	EPwm8Regs.ETCLR.bit.INT = 1;					//  for interrupt at PRD		-->> Double sampling
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;

}
/*------------------------------------------------------------------*/
/*         End of OFFSET.C											*/
/*------------------------------------------------------------------*/
