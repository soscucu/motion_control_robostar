#include <math.h>
#include "DSP2834x_Device.h"     // DSP2833x Headerfile Include File
#include "DSP2834x_Examples.h"   // DSP2833x Examples Include File
#include "MPLD.h"
#include "da.h"
#include "cc.h"
#include "comp.h"
#include "variable.h"
#include "filter.h"
#include "fault.h"
#include "sc.h"


extern interrupt void sc(void);


void UpdateSCGains(Motor *tmpM, SC *tmpSC);
void ResetSC(SC *tmpSC);
void SpdCtrl(SC *tmpSC, Motor *tmpM);
void SpdCtrl_inv(SC *tmpSC, Motor *tmpM);
void MtpaOnlyq(SC *tmpSC, Motor *tmpM);
void InitSCVars(SC *tmpS, float slope, float TeMax, float IqMax, float Wc);

	
int sc_count = 0;
interrupt void sc(void)
{

//	PieCtrlRegs.PIEACK.all = PIEACK_GROUP6;
	IERBackupSC = IER;
//	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3;		// Fault, CC int Enable 1080
	IER = M_INT1|M_INT12|M_INT3|M_INT5;			/* HW fault (XINT1,XINT2,XINT3,XINT4,XINT5), CC */
	EINT;   /* Enable Global interrupt INTM */
 

	SC_num++;
	Time_SL_ave_add+=Time_SL_ave;
	if(SC_num==1000)
	{
		Time_SL_ave_ave = Time_SL_ave_add * 0.001;
		Time_SL_ave_add=0.;
		SC_num=0;
	}


	PieCtrlRegs.PIEACK.all = PIEACK_GROUP6;
	IER = IERBackupSC; 
	
}




void InitSCVars(SC *tmpS, float slope, float TeMax, float IqMax, float Wc) 
{

	
	tmpS->Wrm = 0.; tmpS->Wr= 0.; tmpS->Wrpm= 0.;
	tmpS->Wrm_ref=0. ;tmpS->Wrm_ref_old=0. ; tmpS->Wr_ref=0. ; tmpS->Wrpm_ref=0. ; tmpS->Wrpm_ref_set=0.;
	tmpS->Te_ref_fb = 0. ; tmpS->Te_ref_ff = 0.; 
	tmpS->Te_ref= 0.; tmpS->Te_ref_integ = 0.; tmpS->Te_real=0.; tmpS->Te_Anti=0.; tmpS->Err_Wrm = 0.;
	tmpS->Ki_sc = 0.; tmpS->Ki_scT=0. ; tmpS->Kp_sc=0; tmpS->Ka_sc=0.; tmpS->alpha_sc=1 ;
	
	tmpS->RefSlope = slope;// rpm/sec
	tmpS->Te_ref_max = TeMax; tmpS->Iqse_ref_max = IqMax;
	tmpS->Wc_sc = Wc;
	tmpS->Te_ref_set=0.;
	tmpS->Iqse_ref_set =0.;
}


void UpdateSCGains(Motor *tmpM, SC *tmpSC)
{
	tmpSC->Kp_sc = tmpM->Jm * tmpSC->Wc_sc; 
	tmpSC->Ki_sc = 0.2 * tmpSC->Kp_sc * tmpSC->Wc_sc;
	tmpSC->Ki_scT = tmpSC->Ki_sc * Tsc;
	tmpSC->Ka_sc = 1./ tmpSC->Kp_sc;
}



void ResetSC(SC *tmpSC)
{	
	tmpSC->Te_ref_integ = 0.; 
	tmpSC->Te_ref =0.;
	tmpSC->Wrpm_ref = tmpSC->Wrpm ;

}

