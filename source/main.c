// TI File $Revision: /main/5 $
// Checkin $Date: February 19, 2009   10:43:59 $
//###########################################################################
//
// FILE:    Example_2834xCpuTimer.c
//
// TITLE:   DSP2834x Device Getting Started Program.
//
// ASSUMPTIONS:
//
//    This program requires the DSP2834x header files.
//
//    Other then boot mode configuration, no other hardware configuration
//    is required.
//
//
//    As supplied, this project is configured for "boot to SARAM"
//    operation.  The 2834x Boot Mode table is shown below.
//    For information on configuring the boot mode of an eZdsp,
//    please refer to the documentation included with the eZdsp,
//
//       $Boot_Table:
//
//         GPIO87   GPIO86     GPIO85   GPIO84
//          XA15     XA14       XA13     XA12
//           PU       PU         PU       PU
//        ==========================================
//            1        1          1        1    TI Test Only
//            1        1          1        0    SCI-A boot
//            1        1          0        1    SPI-A boot
//            1        1          0        0    I2C-A boot timing 1
//            1        0          1        1    eCAN-A boot timing 1
//            1        0          1        0    McBSP-A boot
//            1        0          0        1    Jump to XINTF x16
//            1        0          0        0    Jump to XINTF x32
//            0        1          1        1    eCAN-A boot timing 2
//            0        1          1        0    Parallel GPIO I/O boot
//            0        1          0        1    Parallel XINTF boot
//            0        1          0        0    Jump to SARAM	    <- "boot to SARAM"
//            0        0          1        1    Branch to check boot mode
//            0        0          1        0    I2C-A boot timing 2
//            0        0          0        1    Reserved
//            0        0          0        0    TI Test Only
//                                              Boot_Table_End$$
//
// DESCRIPTION:
//
//    This example configures CPU Timer0, 1, and 2 and increments
//    a counter each time the timers assert an interrupt.
//
//       Watch Variables:
//          CpuTimer0.InterruptCount
//          CpuTimer1.InterruptCount
//          CpuTimer2.InterruptCount
//
//###########################################################################
// $TI Release: DSP2834x C/C++ Header Files V1.10 $
// $Release Date: July 27, 2009 $
//###########################################################################


#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include "easy2834x_sci_v7.4.h"
#include "easy2834x_spi_flash.h"
#include "DSP2834x_Gpio.h"
#include "MPLD.h"
#include "da.h"
#include "variable.h"
#include "fault.h"
#include "comp.h"
#include "cc.h"
#include "sc.h"
//#include "shunt.h"
//#include "adc.h"

// Prototype statements for functions found within this file.
//extern interrupt void cc(void);
extern interrupt void fault(void);
extern interrupt void offset(void);
extern interrupt void sc(void);
extern interrupt void eqep_offset(void);

void InitPara(void);
void UpdatePara(void);

FAULT_REG Fault;
//Inductance finding
float test_fL_define = 1500;
//IF
void main(void)
{	

// Step 1. Initialize System Control:
// PLL, WatchDog, enable Peripheral Clocks
// This example function is found in the DSP2834x_SysCtrl.c file.

	InitSysCtrl();

	InitGpio();
/*
    EALLOW;

	GpioCtrlRegs.GPAMUX1.all = 0x00000000;     // GPIO functionality GPIO0-GPIO15
	GpioCtrlRegs.GPADIR.all = 0x00005F90;      // GPIO0-GPIO31 are inputs
	GpioCtrlRegs.GPAQSEL1.all = 0x0000;    // GPIO0-GPIO15 Synch to SYSCLKOUT 
	GpioCtrlRegs.GPAPUD.all = 0x0000;      // Pullup's enabled GPIO0-GPIO31
	EDIS;
 */


// Step 2. Initalize GPIO:
// This example function is found in the DSP2834x_Gpio.c file and
// illustrates how to set the GPIO to it's default state.
// InitGpio();  // Skipped for this example


// Step 3. Clear all interrupts and initialize PIE vector table:
// Disable CPU interrupts
   DINT;

// Initialize the PIE control registers to their default state.
// The default state is all PIE interrupts disabled and flags
// are cleared.
// This function is found in the DSP2834x_PieCtrl.c file.
   InitPieCtrl();

// Disable CPU interrupts and clear all CPU interrupt flags:
   IER = 0x0000;
   IFR = 0x0000;

// Initialize the PIE vector table with pointers to the shell Interrupt
// Service Routines (ISR).
// This will populate the entire table, even if the interrupt
// is not used in this example.  This is useful for debug purposes.
// The shell ISR routines are found in DSP2834x_DefaultIsr.c.
// This function is found in DSP2834x_PieVect.c.
   InitPieVectTable();

// Interrupts that are used in this example are re-mapped to
// ISR functions found within this file.
   EALLOW; // This is needed to write to EALLOW protected registers


	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;		// Enable the PIE block


	EDIS;    // This is needed to disable write to EALLOW protected registers

				//
	EALLOW;

	// XINT1 as Current fault
	// XINT2 as Vdc fault


	XIntruptRegs.XINT1CR.all = 0x01;
	XIntruptRegs.XINT2CR.all = 0x01;
//	XIntruptRegs.XINT3CR.all = 0x01;
//	XIntruptRegs.XINT4CR.all = 0x01;
//	XIntruptRegs.XINT5CR.all = 0x01;
	GpioIntRegs.GPIOXINT1SEL.bit.GPIOSEL = 15; //nINT_Shunt
	GpioIntRegs.GPIOXINT2SEL.bit.GPIOSEL = 27; //nINT_Vdc
//	GpioIntRegs.GPIOXINT3SEL.bit.GPIOSEL = 21; //nFault_senIabc
//  GpioIntRegs.GPIOXINT4SEL.bit.GPIOSEL = 22; //nFault_senIuve
//	GpioIntRegs.GPIOXINT5SEL.bit.GPIOSEL = 24; //nFault_Vdc
//	GpioIntRegs.GPIOXINT1SEL.bit.GPIOSEL = 15; //nINT_Shunt
//	GpioIntRegs.GPIOXINT3SEL.bit.GPIOSEL = (53 - 32); //nFLTI0
//	GpioIntRegs.GPIOXINT4SEL.bit.GPIOSEL = (54 - 32); //nFLTI1
//	GpioIntRegs.GPIOXINT5SEL.bit.GPIOSEL = (33 - 32); //nINT_Vdc


	   
	/* External Fault Interrupt*/
	
	PieVectTable.XINT1 = &fault;
	PieVectTable.XINT2 = &fault;
//	PieVectTable.XINT3 = &fault;
//	PieVectTable.XINT4 = &fault;
//	PieVectTable.XINT5 = &fault;

	PieCtrlRegs.PIEIER1.bit.INTx4 = 1;
	PieCtrlRegs.PIEIER1.bit.INTx5 = 1;
//	PieCtrlRegs.PIEIER12.bit.INTx1 = 1; 
//	PieCtrlRegs.PIEIER12.bit.INTx2 = 1; 
//	PieCtrlRegs.PIEIER12.bit.INTx3 = 1;
	
	
	   
    

	/* Trip Zone Interrupt*/	
	PieVectTable.EPWM1_TZINT = &fault;
//	PieVectTable.EPWM2_TZINT = &fault;
//	PieVectTable.EPWM3_TZINT = &fault;
//	PieVectTable.EPWM4_TZINT = &fault;
//	PieVectTable.EPWM5_TZINT = &fault;
//	PieVectTable.EPWM6_TZINT = &fault;
	
	PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
//	PieCtrlRegs.PIEIER2.bit.INTx2 = 1;
//	PieCtrlRegs.PIEIER2.bit.INTx3 = 1;
//	PieCtrlRegs.PIEIER2.bit.INTx4 = 1;
//	PieCtrlRegs.PIEIER2.bit.INTx5 = 1;
//	PieCtrlRegs.PIEIER2.bit.INTx6 = 1;

	/* For double sampling, Peak, Valley */
	PieVectTable.EPWM7_INT = &offset;
	PieVectTable.EPWM8_INT = &offset;
	PieCtrlRegs.PIEIER3.bit.INTx7 = 1;
	PieCtrlRegs.PIEIER3.bit.INTx8 = 1;
	 
	PieVectTable.EQEP2_INT = &eqep_offset;
	PieCtrlRegs.PIEIER5.bit.INTx2 = 1;
	

    // shunt/

	/* start */
	IER |= M_INT1|M_INT2|M_INT3|M_INT9|M_INT12|M_INT6|M_INT5;			// HW fault || TZ || CC || EasyDSP
//	IER |= M_INT1|M_INT2|M_INT3|M_INT9;			// HW fault || TZ || CC || EasyDSP

	EDIS;    // This is needed to disable write to EALLOW protected registers

	EINT;   // Enable Global interrupt INTM
	ERTM;   // Enable Global realtime interrupt DBGM


	Tsw = 1./Fs_set;
	uTsPerTdsp = (unsigned int)(Tsw*0.5*SYSTEM_CLOCK); // 	Ts/2 -> PWM Peak
	INVuTsPerTdsp = 1/uTsPerTdsp;
	Tsamp = ((float)uTsPerTdsp) /SYSTEM_CLOCK;
	Fsamp = 1./Tsamp;
	uHalf_TsPTdsp = uTsPerTdsp / 2.;   


	atanTableInit();

	initEnc(&encoder2, 2500, 4);
	InitEQep();
									
	InitCpuTimers();

	/* For Time_CC */
    CpuTimer2Regs.PRD.all  = SYSTEM_CLOCK;	
    CpuTimer2Regs.TCR.bit.TSS = 0;
    CpuTimer2Regs.TCR.bit.TRB = 1;	

	InitXintf16Gpio();
	InitXintf();

	InitAdc();
	InitSerialDac();
	easyDSP_SCI_Init();
	easyDSP_SPI_Flashrom_Init();
	InitEpwm();					
	InitEPwmGpio();

	Obs1.Fso = 15.;
	InitObs(&Obs1, 2*PI*Obs1.Fso, Inv1.Jm); 

 

	InitFault();
	InitPara();


	InitSCVars(&Inv1_SC, 150, Inv1.Te_rated * 1.0, Inv1.Is_rated, 2 * PI * Fsc);
	
	Obsd.Fdis = 200.;
	Obsd.Wdis = 2*PI*Obsd.Fdis;

	InitDistObs(&Obsd, Obsd.Wdis);
	InitTDOF(&TDOFV, 2 * PI*0.005);
   	while(1)
	{
// 		UpdateGainObs(&Obs1);
		UpdateGainObs(&Inv1, &Obs1);
		UpdateGainObsd(&Obsd);
		UpdateGainTDOF(&TDOFV);
 		UpdatePara();
		
		if(CLR_Fault)	{
			InitFault();
			InitPara();
			ResetCC1(); 
			CLR_Fault = 0;
		}
	}
}

void InitPara(){
	/* system para */
	InitParameters1();

	InitProt();
}

float alpha = 0.2;
void UpdatePara(){
	Inv1.Wcc = 2 * PI * Inv1.Fcc ;

	Inv1.Kpd = Inv1.Ls * Inv1.Wcc;
	Inv1.Kpq = Inv1.Ls * Inv1.Wcc;
	

	Inv1.Kid_T = Inv1.Rs * Inv1.Wcc * Tsamp;
	Inv1.Kiq_T = Inv1.Rs * Inv1.Wcc * Tsamp;


	Inv1.Kad = 1 / Inv1.Kpd;
	Inv1.Kaq = 1 / Inv1.Kpq;

	Inv1.Wsc = 2 * PI * Inv1.Fsc ;

	Inv1.Kp_sc = 2*Inv1.zeta_sc*Inv1.Wsc*alpha*Inv1.INV_Kt*Inv1.Jm;
	Inv1.Ki_sc = Inv1.Wsc*alpha*Inv1.Wsc*alpha*Inv1.INV_Kt*Inv1.Jm;
	  
	Inv1.Ki_scT = Inv1.Ki_sc * Tsamp; 
	Inv1.Ka_sc = 1 / Inv1.Kp_sc;
	
	Inv1.alpha = 1;
	
	
	Inv1.Wpc = 2 * PI * Inv1.Fpc;


	Inv1.Kp_pc = Inv1.Wpc;
	Inv1.Kd_pc = Inv1.Wpc / Inv1.Wsc;
	Inv1.Kd_pcT = Inv1.Kd_pc * Fsamp;
	Inv1.InvJm = 1/Inv1.Jm;



}

//===========================================================================
// No more.
//===========================================================================


 
