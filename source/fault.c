/******************************************************************************
    FAULT.C
******************************************************************************/
#include "DSP2834x_Device.h"    
#include "DSP2834x_Examples.h"
#include "MPLD.h"
#include "da.h"
#include "variable.h"
#include "fault.h"
#include "cc.h"
#define ON 1
#define OFF 0

int faultDispMode = 0, faultData = 0;
int fault_done = 0;
long Flag_Analog_fault=0, Flag_IPM1_fault=0, Flag_IPM2_fault=0, Flag_SW_fault=0;
int faulttest = 0;
int flta=0;
int abcd=0, abcde=0;


void FaultProcess(void);
void InitFault(void);

interrupt void fault(void)
{   

	flta++;
//	RELAY_OFF;
//	PieCtrlRegs.PIECTRL.bit.ENPIE = 0;
//	DINT;

	if (GpioDataRegs.GPADAT.bit.GPIO15 == 0)	{ Fault.HW.bit.Current = 1;}

	if (GpioDataRegs.GPADAT.bit.GPIO27 == 0)	{ Fault.HW.bit.Vdc = 1;}


//    if (GpioDataRegs.GPBDAT.bit.GPIO53 == 0)     {}  
	FaultProcess();

//	fault_Vdc = Vdc;
//	fault_Vn = Vn;
//	fault_Ias1 = Ias1;
//	fault_Ibs1 = Ibs1;
//	fault_Ics1 = Ics1;
//	fault_Ias2 = Ias2;
//	fault_Ibs2 = Ibs2;
//	fault_Ics2 = Ics2;


	IER &= 0x0000;
	
	EALLOW;


	if (EPwm1Regs.TZFLG.bit.INT == 1)
	{
		EPwm1Regs.TZCLR.bit.CBC = 1;
		EPwm1Regs.TZCLR.bit.INT = 1;

		EPwm2Regs.TZCLR.bit.CBC = 1;
		EPwm2Regs.TZCLR.bit.INT = 1;

		EPwm3Regs.TZCLR.bit.CBC = 1;
		EPwm3Regs.TZCLR.bit.INT = 1;

		EPwm4Regs.TZCLR.bit.CBC = 1;
		EPwm4Regs.TZCLR.bit.INT = 1;

		EPwm5Regs.TZCLR.bit.CBC = 1;
		EPwm5Regs.TZCLR.bit.INT = 1;

		EPwm6Regs.TZCLR.bit.CBC = 1;
	 	EPwm6Regs.TZCLR.bit.INT = 1;

		EPwm7Regs.TZCLR.bit.CBC = 1;
		EPwm7Regs.TZCLR.bit.INT = 1;

		EPwm8Regs.TZCLR.bit.CBC = 1;
		EPwm8Regs.TZCLR.bit.INT = 1;

		EPwm9Regs.TZCLR.bit.CBC = 1;
	 	EPwm9Regs.TZCLR.bit.INT = 1;
	}

 	LED1_OFF;

//	GpioDataRegs.GPBCLEAR.bit.GPIO52 = 1;	// To cut-off relay
  
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;    // XINT1,2 flag clear
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;   // TZ flag clear
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;  // XINT3,4,5,6,7 flag clear



	IER = M_INT1|M_INT2|M_INT3|M_INT9|M_INT12;			// HW fault || TZ int || CC || EasyDSP || XINT 3,4,5,6,7

	EDIS;

	EINT;   // Enable Global interrupt INTM
    



	
}

void FaultProcess(void)
{   

	SW1.CC_on = OFF;
	SW1.PC_on = OFF;
	SW1.SC_on = OFF;
	SW1.Align_on = OFF;

/*	EPwm1Regs.AQSFRC.bit.OTSFA = 1;
	EPwm1Regs.AQSFRC.bit.OTSFB = 1;
	EPwm2Regs.AQSFRC.bit.OTSFA = 1;
	EPwm2Regs.AQSFRC.bit.OTSFB = 1;
	EPwm3Regs.AQSFRC.bit.OTSFA = 1;
	EPwm3Regs.AQSFRC.bit.OTSFB = 1;
	EPwm4Regs.AQSFRC.bit.OTSFA = 1;
	EPwm4Regs.AQSFRC.bit.OTSFB = 1;
	EPwm5Regs.AQSFRC.bit.OTSFA = 1;
	EPwm5Regs.AQSFRC.bit.OTSFB = 1;
	EPwm6Regs.AQSFRC.bit.OTSFA = 1;
	EPwm6Regs.AQSFRC.bit.OTSFB = 1;

	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm2Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm3Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm4Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm4Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm5Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm5Regs.AQCTLA.bit.CAD = AQ_CLEAR;
	EPwm6Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
	EPwm6Regs.AQCTLA.bit.CAD = AQ_CLEAR;

    
	EPwm1Regs.CMPA.half.CMPA = 0;
	EPwm2Regs.CMPA.half.CMPA = 0;
	EPwm3Regs.CMPA.half.CMPA = 0;
	EPwm4Regs.CMPA.half.CMPA = 0;
	EPwm5Regs.CMPA.half.CMPA = 0;
	EPwm6Regs.CMPA.half.CMPA = 0;
*/
	Switch1Off();




}

void InitFault(void)
{
	Fault.HW.all = 0x0;
	Fault.SW.all = 0x0;
}
