#ifndef	_SC_H_
#define	_SC_H_

#include "variable.h"
#include "cc.h"


typedef struct 
{ 
	float	Wrm, Wr, Wrpm;
	float	Wrm_ref,Wrm_ref_old, Wr_ref, Wrpm_ref, Wrpm_ref_set,Wrpm_ref_diff ;
	float	Te_ref_fb, Te_ref_ff, Te_ref, Te_ref_set, Te_ref_integ, Te_real, Te_Anti;
	float	Err_Wrm, Iqse_ref_set,Idse_ref_set;
	float	Ki_sc, Ki_scT, Kp_sc, Ka_sc, alpha_sc ;
	float 	RefSlope ;
	float 	Te_ref_max , Iqse_ref_max, Idqse_ref_max;
	float 	Wc_sc ;
}SC; 

extern SC Inv1_SC;
//extern SC Inv2_SC;



extern void InitSCVars(SC *tmpS, float slope, float TeMax, float IqMax, float Wc);
extern void UpdateSCGains(Motor *tmpM, SC *tmpS);
extern void ResetSC(SC *tmpSC);

extern void SpdCtrl(SC *tmpSC, Motor *tmpM);
extern void SpdCtrl_inv(SC *tmpSC, Motor *tmpM);
extern void MtpaOnlyq(SC *tmpSC, Motor *tmpM);




#endif
