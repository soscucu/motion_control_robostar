/***************************************************************
	FILTER.H
	generic 1st- and 2nd-order filter
	
	1st-order filter : 
	void IIR1CoeffInit(IIR1 *p_gIIR, const dt);
	void IIR1Init(IIR1 *p_gIIR, const float sample_time);
	float IIR1Update(IIR1 *p_gIIR, const float input);

	2nd-order filter : 
	void IIR2CoeffInit(IIR1 *p_gIIR, const dt);
	void IIR2Init(IIR2 *p_gIIR, const float sample_time);
	float IIR2Update(IIR2 *p_gIIR, const float input);

	programmed by Yo-chan Son, Oct 2000.
	copyright (c) 1991, 2000 by EEPEL, SNU, SEOUL, KOREA
	All Rights Reserved.

	modified by T.S.Kwon, 2008
	1st-order filter : 
	void IIR1CoeffInit(IIR1 *p_gIIR, float w0 );
	void IIR1Init(IIR1 *p_gIIR, float w0 );
	float IIR1Update(IIR1 *p_gIIR, const float input );

	2nd-order filter : 
	void IIR2CoeffInit(IIR1 *p_gIIR, float w0, float zeta );
	void IIR2Init(IIR2 *p_gIIR, float w0, float zeta );
	float IIR2Update(IIR2 *p_gIIR, const float input);
****************************************************************/
#ifndef _CUSTOMIZED_FILTER__
#define _CUSTOMIZED_FILTER__

#define	K_ALLPASS	0
#define	K_LPF		1
#define	K_HPF		2
#define	K_BPF		3
#define	K_NOTCH		4

#define IIR1DEFINE(type, w0, Tsam)		{(int)(type), (float)(w0), (float)(Tsam), 0, 0, 0, 0}
#define IIR2DEFINE(type, w0, zeta,Tsam)	{(int)(type), (float)(w0), (float)(zeta), (float)(Tsam), 0, 0, 0, 0, 0, 0, 0}

typedef struct	{
	int type;
	float w0;
	float delT;
	float coeff[3], reg;
}	IIR1;

typedef struct	{
	int type;
	float w0, zeta;
	float delT;
	float coeff[5], reg[2];
}	IIR2;

/*void IIR1CoeffInit(IIR1 *p_gIIR, const float dt);
void IIR1Init(IIR1 *p_gIIR, const float dt);
float IIR1Update(IIR1 *p_gIIR, const float x);*/
void initiateIIR1(IIR1 *p_gIIR, int type, float w0, float Tsam);
void initiateIIR2(IIR2 *p_gIIR, int type, float w0, float zeta, float Tsam);
void UpdateGainIIR1(IIR1 *p_gIIR, int type, float w0, float Tsam);
void UpdateGainIIR2(IIR2 *p_gIIR, int type, float w0, float zeta, float Tsam);


void IIR1CoeffInit(IIR1 *p_gIIR, float w0);
void IIR1Init(IIR1 *p_gIIR, float w0);
float IIR1Update(IIR1 *p_gIIR, float input);

/*void IIR2CoeffInit(IIR2 *p_gIIR, const float dt);
void IIR2Init(IIR2 *p_gIIR, const float dt);
float IIR2Update(IIR2 *p_gIIR, const float x);*/
void IIR2CoeffInit(IIR2 *p_gIIR, float w0, float zeta );
void IIR2Init(IIR2 *p_gIIR, float w0, float zeta );
float IIR2Update(IIR2 *p_gIIR, const float x);
//float IIR2Update(IIR2 *p_gIIR, const float input);

//extern IIR1 Filter_Vdc1 ;
//extern IIR2 Filter_Vdc2 ;

extern IIR1	Filter_WrH1;
extern IIR1	Filter_WrH2;
extern IIR1 Filter_Edse;
extern IIR1 Filter_Eqse;
extern IIR1 Filter_Vdiff;

extern IIR2 Filter_Idseh;

extern IIR2 Filter_Iqseh;
//extern IIR1 Filter_Idse;
extern IIR2 Filter_Idse;
extern IIR2 Filter_Iqse;

extern IIR1 Filter_Vdse;
extern IIR1 Filter_Vqse;
extern IIR1 Filter_Vdse_flt;
extern IIR1 Filter_Vqse_flt;
//extern IIR1 Filter_Iqse;
extern IIR1 Filter_Idc_sen;

extern IIR1 Filter_Idss;
extern IIR1 Filter_Iqss;
extern IIR1 Filter_Idsc;
extern IIR1 Filter_Iqsc;
extern IIR1 Filter_Ke_d;
extern IIR1 Filter_Ke_q;
extern IIR2 Filter_test;
extern IIR2 Filter_Pt;
extern IIR2 Filter_Pinv;

extern IIR1 Filter_Ke_d_sum;
extern IIR1 Filter_Ke_q_sum;

extern IIR2 Filter_J_find;
#endif

