/****************************************************
MPLD.h
*****************************************************/

#ifndef _MPLD_H__
#define _MPLD_H__
#define SYSTEM_CLOCK	((float)300.e6)
#define SYSTEM_CLOCK_PRD	((float)0.00333333333333333e-6)
#define CARR_SHIFT		1
#define SIN_LUT_SIZE	8192.
#define XINTF_ZONE6		0x100000
#define XINTF_ZONE7		0x200000

//#define MPLD_ADDR		XINTF_ZONE7
#define MPLD_ADDR	XINTF_ZONE7

#define LELAY_AD_ON   (*(volatile int *)(MPLD_ADDR + 0x0008))

#define LELAY_AD_ON1   (*(volatile int *)(MPLD_ADDR + 0x000E))


#define AD_BASE			(MPLD_ADDR + 0x1000)
	#define	AD_CS_BASE		(AD_BASE + 0x0000)
		#define	AD_RD0			(*(volatile int *)(AD_CS_BASE + 0x0000))
		#define	AD_RD1			(*(volatile int *)(AD_CS_BASE + 0x0002))
		#define	AD_RD2			(*(volatile int *)(AD_CS_BASE + 0x0004))
//		#define	AD_RD3			(*(volatile int *)(AD_CS_BASE + 0x0006))
//		#define	AD_RD4			(*(volatile int *)(AD_CS_BASE + 0x0008))
//		#define	AD_RD5			(*(volatile int *)(AD_CS_BASE + 0x000A))
//		#define	AD_RD6			(*(volatile int *)(AD_CS_BASE + 0x000C))
//		#define	AD_RD7			(*(volatile int *)(AD_CS_BASE + 0x000E))
//		#define	AD_RD8			(*(volatile int *)(AD_CS_BASE + 0x0010))
//		#define	AD_RD9			(*(volatile int *)(AD_CS_BASE + 0x0012))
//		#define	AD_RD10			(*(volatile int *)(AD_CS_BASE + 0x0014))
//		#define	AD_RD11			(*(volatile int *)(AD_CS_BASE + 0x0016))
	#define	AD_SOC_BASE		(AD_BASE + 0x0100)
		#define	AD_RST	    (*(volatile long *)(AD_SOC_BASE + 0x0000))
		#define AD_CV	    	(*(volatile int *)(AD_SOC_BASE + 0x0002))
	#define AD_RD_BASE	    (AD_BASE + 0x0200)
		#define AD_BS 		(*(volatile long *)(AD_RD_BASE + 0x0000))

/************************************************************/
/*	System Clock											*/
/************************************************************/
extern float MPLD_clock, XPLD_clock;		// Clock frequency
extern float MPLD_period, XPLD_period;		// Clock period


#endif	// of _MPLD_H__



