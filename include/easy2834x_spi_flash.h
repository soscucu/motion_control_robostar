/***************************************************************
    spi_flash.h
	copyright (c) 2010 by Dae-Woong Chung
	All Rights Reserved.
****************************************************************/
#ifndef _SPI_FLASH_H__
#define _SPI_FLASH_H__

extern void easyDSP_SPI_Flashrom_Init(void);

#endif	// of _SPI_FLASH_H__
