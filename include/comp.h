/***************************************************************
	COMP.H
	
	programmed by Yo-chan Son, Oct 2000.
	copyright (c) 1991, 2000 by EEPEL, SNU, SEOUL, KOREA
	All Rights Reserved.
****************************************************************/

#ifndef	_CUSTOMIZED_COMP
#define	_CUSTOMIZED_COMP

#define	OP_BR		0x60000000

// Inverse Factorial 
#define f2      	((float)(1./2.))
#define	f3			((float)(f2/3.))
#define	f4			((float)(f3/4.))
#define	f5			((float)(f4/5.))
#define	f6			((float)(f5/6.))
#define	f7			((float)(f6/7.))
#define	f8			((float)(f7/8.))
#define	f9			((float)(f8/9.))
#define	f10			((float)(f9/10.))
#define	f11			((float)(f10/11.))
#define	f12			((float)(f11/12.))
#define	f13			((float)(f12/13.))
#define	f14			((float)(f13/14.))
#define	f15			((float)(f14/15.))

//#define TRUE		1
// Floating-point Constants
#define	PI			((float)3.1415926535897932384626433832795)
#define	SQRT2		((float)1.414213562373095)
#define	SQRT3		((float)1.732050807568877)
#define	INV_SQRT3	((float)0.577350269189626)
#define	INV_SQRT2	((float)0.707106781186547)
#define	INV_3		((float)0.333333333333333)
#define INV_2PI		((float)0.15915494309189533576888376337251)
#define Rm2Rpm		((float)9.5492965855137201461330258023509)	
#define Rpm2Rm		((float)0.10471975511965977461542144610932)

#define	INV_PI		((float)0.318309886183791)
#define	PI_INV_2	(PI/2.)
#define	PI_INV_6	(PI/6.)
#define	PI5_INV_6	(PI*5./6.)
#define PI_INV_3	(PI/3.)
#define	PI2_INV_3	(PI*2./3.)

#define PI2_9		((float)0.698131700797732)	//(2*PI/9)
#define PI4_9		((float)1.396263401595464)	//(4*PI/9)
#define SIN_2PI_9	((float)0.642787609686539)	//SIN(2*PI/9)
#define COS_2PI_9	((float)0.766044443118978)	//COS(2*PI/9)
//#define SIN_4PI_9	((float)0.984807753012208)	//SIN(4*PI/9)
//#define COS_4PI_9	((float)0.173648177666930)	//COS(4*PI/9)
#define SIN_4PI_9	((float)0.9842)	//SIN(4*PI/9)
#define COS_4PI_9	((float)0.1733)	//COS(4*PI/9)

#if 0
#define SIN_2PI_9	((float)0.984807753012208)	//SIN(2*PI/9)
#define COS_2PI_9	((float)0.173648177666930)	//COS(2*PI/9)
#define SIN_4PI_9	((float)0.642787609686539)	//SIN(4*PI/9)
#define COS_4PI_9	((float)0.766044443118978)	//COS(4*PI/9)
#endif

#define SIN_2PI_3	((float)0.866025403784439)	//SIN(2*PI/9)
#define COS_2PI_3	((float)-0.500000000000000)	//COS(2*PI/9)
#define SIN_4PI_3	((float)-0.866025403784438)	//SIN(4*PI/9)
#define COS_4PI_3	((float)-0.500000000000000)	//COS(2*PI/9)

#define INV_6		0.16666666667
//#define INV_3 	0.33333333333
#define INV_2_3	0.66666666667
#define INV_24	0.04166666667
#define INV_4		0.25
#define INV_12	0.08333333333
#define INV_3_2	1.5
#define INV_3_4 0.75
#define INV_3_8 0.375
#define INV_4_3TH	0.015625
#define INV_4_3 1.33333333333
#define F2		1./2.

// Macro Functions
//#define	BOUND_PI(x)		((x) - 2.*PI*(int)((x + PI)/(2.*PI)))
//#define BOUND_PI(x)		((x>0)?((x)-2.*PI*(int)((x+PI)/(2.*PI))):((x)-2.*PI*(int)((x-PI)/(2.*PI))))
#define BOUND_PI(x)		((x>0)?((x)-2.*PI*(long)((x+PI)*INV_2PI)):((x)-2.*PI*(long)((x-PI)*INV_2PI)))

#define BOUND_PI2(x)	((x>0)?((x)-2.*PI*(int)((x)*INV_2PI)):((x)-2.*PI*(int)((x-2*PI )*INV_2PI)))

#define SIN(x,x2)		((x)*((float)1.-(x2)*(f3-(x2)*(f5-(x2)*(f7-(x2)*(f9-(x2)*(f11-(x2)*(f13-(x2)*f15))))))))
#define COS(x2)			((float)1.-(x2)*(f2-(x2)*(f4-(x2)*(f6-(x2)*(f8-(x2)*(f10-(x2)*(f12-(x2)*f14)))))))
#define SIN_INV_X(x2)   (((float)1.-(x2)*(f3-(x2)*(f5-(x2)*(f7-(x2)*(f9-(x2)*(f11-(x2)*(f13-(x2)*f15))))))))
#define EXP(x)			((float)1.+(x)*((float)1.+(x)*(f2+(x)*(f3+(x)*(f4+(x)*(f5+(x)*(f6+(x)*f7)))))))
#define ABS(x)			(((x)>0)? (x) : (-(x)))
// 2-D Arctangent Approximation
#define	ATAN_TABLE_SIZE	2048
#define	ATAN_TABLE(f)	(*(float *)(p_atanTable + (int)(f*(float)atanSize + 0.5)))
#define	atan1Table(f)	ATAN_TABLE((f))

int atanTableInit(void);
float atan2Table(const float y, const float x);
float SIGN(const float x);




#endif
