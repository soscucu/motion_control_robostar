/*****************************************************************************
    DA.H
******************************************************************************/
/*	#ifndef DA_ADDR
	#define	DA_ADDR		0X819000
	#endif

	#ifndef XDA_ADDR
	#define	XDA_ADDR	0X109000
	#endif

	#define DA_LSB(ch)	(*(volatile int *)(DA_ADDR + ((ch) << 1)))
	#define DA_MSB(ch)	(*(volatile int *)(DA_ADDR + ((ch) << 1) + 0x01))

	#define DA0_LSB		DA_LSB(0)
	#define	DA0_MSB		DA_MSB(0)
	#define DA1_LSB		DA_LSB(1)
	#define	DA1_MSB		DA_MSB(1)
	#define DA2_LSB		DA_LSB(2)
	#define	DA2_MSB		DA_MSB(2)
	#define DA3_LSB		DA_LSB(3)
	#define	DA3_MSB		DA_MSB(3)
	#define DA_LATCH	(*(volatile int *)(DA_ADDR + 0x0F))

	#define XDA_LSB(ch)	(*(volatile int *)(XDA_ADDR + ((ch) << 1)))
	#define XDA_MSB(ch)	(*(volatile int *)(XDA_ADDR + ((ch) << 1) + 0x01))

	#define XDA0_LSB	XDA_LSB(0)
	#define	XDA0_MSB	XDA_MSB(0)
	#define XDA1_LSB	XDA_LSB(1)
	#define	XDA1_MSB	XDA_MSB(1)
	#define XDA2_LSB	XDA_LSB(2)
	#define	XDA2_MSB	XDA_MSB(2)
	#define XDA3_LSB	XDA_LSB(3)
	#define	XDA3_MSB	XDA_MSB(3)
	#define XDA_LATCH	(*(volatile int *)(XDA_ADDR + 0x0F))
*/
extern long *da[4];
extern long *test_tw[1];

extern int  da_type[4], da_data[4];
extern float da_scale[4], da_mid[4], da_temp[4];
//extern float da_test;

void InitSerialDac(void);
void SerialDacOut(void);

  
#define DACOUT(x)	{da_temp[(x)] = da_type[(x)] ? (float)(*da[(x)]) : *(float *)(da[(x)]);\
	da_data[(x)] = (int)(((long)(- da_scale[(x)] * (da_temp[(x)] - da_mid[(x)]))) & 0x0FFF);\
	SpidRegs.SPITXBUF = 0x1000|((x)<<9) ;  SpidRegs.SPITXBUF =(int)(da_data[(x)]<<4)&0xFF00;\
	SpidRegs.SPITXBUF =(int)(da_data[(x)]<<12)&0xFF00; }
/*
#define DACOUT(x)	{da_temp[(x)] = da_type[(x)] ? (float)(*da[(x)]) : *(float *)(da[(x)]);\
	da_data[(x)] = ((int)(0x800 - da_scale[(x)] * (da_temp[(x)] - da_mid[(x)])))%4096;\
	SpidRegs.SPITXBUF = 0x1000|((x)<<1) ;  SpidRegs.SPITXBUF =(int)(da_data[(x)]<<4)&0xFF00;\
	SpidRegs.SPITXBUF =(int)(da_data[(x)]<<12)&0xFF00; }
*/
